<?php

use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\Entry\SelectController;
use App\Http\Controllers\Frontend\Entry\RAB\RABController;
use App\Http\Controllers\Frontend\ProsesController;
use App\Http\Controllers\Frontend\DocumentController;
use App\Http\Controllers\Frontend\InfoController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        // User Dashboard Specific
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('account');

        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    });

    // Entri
    Route::group(['namespace' => 'Entry', 'as' => 'entry.'], function () {
        // Menu Pilih Entri
        Route::get('entri', 'SelectController')->name('index');

        // RAB atau BOQ
        Route::group(['namespace' => 'RAB', 'as' => 'RAB.'], function () {
            Route::prefix('entri/RAB')->group( function () {
                // Entri Lembar RAB
                Route::get('lembar/{id}',               [RABController::class, 'sheet'])->name('sheet');
                Route::get('lembar/{id}/butir/{num}',   [RABController::class, 'showunits'])->name('sheet.units.show');
                Route::post('lembar/komponen/{id}',     [RABController::class, 'commitcomponents'])->name('sheet.components.commit');
                Route::post('lembar/{id}/butir/{num}',  [RABController::class, 'commitunits'])->name('sheet.units.commit');

                // Detail RAB
                // ------------- index in selectController
                Route::get('detail/buat',       [RABController::class, 'createdetail'])->name('detail.create');
                Route::post('detail',           [RABController::class, 'storedetail'])->name('detail.store');
                // Route::get('detail/{id}',        [RABController::class, 'showdetail'])->name('detail.show'); //readonly
                Route::get('detail/{id}',       [RABController::class, 'editdetail'])->name('detail.edit');
                Route::patch('detail/{id}',     [RABController::class, 'updatedetail'])->name('detail.update');
                Route::get('detail/{id}/delete',    [RABController::class, 'deletedetail'])->name('detail.delete'); 

                // Kirim RAB
                Route::get('kirim/{id}',        [RABController::class, 'send'])->name('send');
                Route::get('batal-kirim/{id}',  [RABController::class, 'unsend'])->name('unsend');
            });
        });
        
    });

    // Download
    Route::group(['namespace' => 'Proses', 'as' => 'proses.'], function () {
        Route::get('proses', [ProsesController::class, 'index'])->name('index');

        // ambil di RAB, buat attribut baru, namanya notvalids[], isinya array json
        Route::get('validate/{id}', [ProsesController::class, 'showValids'])->name('valids.show');
        Route::post('validate/{id}', [ProsesController::class, 'updateValids'])->name('valids.update');

        // Komponen ditambah atribut baru namanya kode_sistem, isinya integer, karena fixed definisi kode_sistem itu
        Route::get('sistem/{id}', [ProsesController::class, 'showSystemCode'])->name('syscode.show'); 
        Route::post('sistem/{id}', [ProsesController::class, 'updateSystemCode'])->name('syscode.update');

        // mendapatkan data boq, diubah menjadi excel, terus upload excel tersebut ke server
        Route::get('boq/{id}', [ProsesController::class, 'showBOQ'])->name('boq.show'); 
        Route::post('boq/{id}', [ProsesController::class, 'storeBOQ'])->name('boq.store');
    });

    // Upload
    Route::group(['namespace' => 'Upload', 'as' => 'upload.'], function () {
        Route::get('upload', [DocumentController::class, 'index'])->name('index');
        Route::post('upload', [DocumentController::class, 'store'])->name('store');
    });
});

    // Informasi
    Route::group(['namespace' => 'Info', 'as' => 'info.'], function () {
        Route::get('info', [InfoController::class, 'index'])->name('index');
    });
