<?php

// Breadcrumbs::for('frontend.index', function ($trail) {
//     $trail->push(__('Halaman Depan'), route('frontend.index'));
// });

Breadcrumbs::for('frontend.index', function ($trail) {
    $trail->push('Halaman Depan', route('frontend.index'));
});

Breadcrumbs::for('frontend.user.account', function ($trail) {
    $trail->push('Akun ku', route('frontend.user.account'));
});


Breadcrumbs::for('frontend.auth.login', function ($trail) {
    $trail->push('Masuk', route('frontend.auth.login'));
});


Breadcrumbs::for('frontend.entry.index', function ($trail) {
    $trail->push(__('Manajemen Entri'), route('frontend.entry.index'));
});

    Breadcrumbs::for('frontend.entry.RAB.sheet', function ($trail, $id) {
        $trail->parent('frontend.entry.index');
        $trail->push(__('Entri RAB'), route('frontend.entry.RAB.sheet', $id));
    });

    Breadcrumbs::for('frontend.entry.RAB.detail', function ($trail) {
        $trail->parent('frontend.entry.index');
        $trail->push(__('Detail RAB'), route('frontend.entry.RAB.detail'));
    });

Breadcrumbs::for('frontend.download.index', function ($trail) {
    $trail->push(__('Download'), route('frontend.download.index'));
});

// For Nested
// Breadcrumbs::for('frontend.download.index', function ($trail) {
//     $trail->parent('frontend.entry.index');
//     $trail->push(__('Download'), route('frontend.download.index'));
// });

Breadcrumbs::for('frontend.upload.index', function ($trail) {
    $trail->push(__('Upload Dokumen'), route('frontend.index'));
});

Breadcrumbs::for('frontend.info.index', function ($trail) {
    $trail->push(__('Informasi Aplikasi'), route('frontend.index'));
});