// $(document).ready(function(){

daftar_kode_analisa_data = JSON.parse($('#daftar-kode-analisa-hi').val())

// Format Angka Ribuan
$('input.harga').number(true, 2);

// constructs the suggestion engine 
var kode = new Bloodhound({
    local: daftar_kode_analisa_data,
    datumTokenizer: Bloodhound.tokenizers.obj.ngram('kode_analisa'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
});

$('#bloodhound .typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
}, {
    name: 'kode_analisa',
    display: 'kode_analisa',
    source: kode,
    templates: {
        empty: [
            '<div class="empty-message">',
            'kode tidak dapat ditemukan, coba kembali',
            '</div>'
        ].join('\n'),
        suggestion: (el) => '<div><strong>' + el.kode_analisa + '</strong> – ' + el.komponen_pekerjaan + '</div>'
    }
});

// $('#kode-analisa-i').on('change', (e) => {
//     let idx = daftar_kode_analisa_data.findIndex((el) => {
//         return (el.kode_analisa === $('#kode-analisa-i').val())
//     })
//     $('#harga-satuan-i').val(daftar_kode_analisa_data[idx].nilai)
// })

$('#kode-analisa-apply-btn').on('click', (e) => {
    let idx = daftar_kode_analisa_data.findIndex((el) => {
        return (el.kode_analisa === $('#kode-analisa-i').val())
    })
    if(idx == -1) { // if not found any
        $('#deskripsi-kode-analisa-label').html('<em>definisi kode analisa tidak dapat ditemukan</em>')
    } else {
        $('#deskripsi-kode-analisa-label').html(daftar_kode_analisa_data[idx].komponen_pekerjaan)
        $('#harga-satuan-i').val(daftar_kode_analisa_data[idx].nilai)
    }
})

$('#hitung-jumlah-harga-btn').on('click', (e) => {
    $('#jumlah-harga-i').val( 
        $.number($('#harga-satuan-i').val() * $('#volume-i').val(),2)
    )
})

// State! just (add, has, delete)
states = new Set()

komponen_terpilih_index = -1 // maksudnya grup komponen 
subkomponen_terpilih_index = -1 // maksudnya komponen dengan unitnya
butir_terpilih_index = -1 // maksudnya komponen dengan unitnya
detail_RAB_data = JSON.parse($('#rab-hi').val())
daftar_butir_raw_data = JSON.parse($('#daftar-butir-hi').val())
daftar_komponen_raw_data = JSON.parse($('#daftar-komponen-raw-hi').val())
daftar_butir_data = () => {
    return daftar_butir_raw_data.filter((el) => !el.hasOwnProperty('deleted')).sort((e,f) => e.nomor-f.nomor)
}
daftar_grup_komponen_data = () => {
    return daftar_komponen_raw_data.filter((el) => !el.hasOwnProperty('deleted')).sort((e,f) => e.urutan_romawi-f.urutan_romawi)
}
daftar_komponen_terpilih_data = () => {
    return (
        komponen_terpilih_index > -1 ?
        daftar_komponen_raw_data[komponen_terpilih_index].components.filter((el) => !el.hasOwnProperty('deleted')).sort((e,f) => e.urutan_abjad-f.urutan_abjad) :
        []
    )
}
komponen_terpilih_id = (num = 1) => {
    return (
        subkomponen_terpilih_index == -1 ?
        num :
        daftar_komponen_raw_data[komponen_terpilih_index].components[subkomponen_terpilih_index].id
    )
}
fraksi_progress = { 'pembilang': 1, 'pembagi': 1 }
persentase_progress = () => {
    let progress = _.round((fraksi_progress.pembilang/fraksi_progress.pembagi)*100)
    return !isNaN(progress) ? progress : 0
}
jumlah_perubahan_pada = (nama_daftar) => {
    let jumlah = 0
    switch (nama_daftar) {
    case 'daftar_komponen_raw_data':
        jumlah += jmespath.search(daftar_komponen_raw_data, "[? contains(to_string(id), 'new')]").length
        jumlah += jmespath.search(daftar_komponen_raw_data, "[].components[? contains(to_string(id), 'new')][]").length
        jumlah += jmespath.search(daftar_komponen_raw_data, "[? edited == `true`]").length
        jumlah += jmespath.search(daftar_komponen_raw_data, "[].components[? edited==`true`][]").length
        jumlah += jmespath.search(daftar_komponen_raw_data, "[? deleted == `true`]").length
        jumlah += jmespath.search(daftar_komponen_raw_data, "[].components[? deleted==`true`][]").length
        return jumlah;
        break
    case 'daftar_butir_raw_data':
        jumlah += jmespath.search(daftar_butir_raw_data, "[? contains(to_string(id), 'new')]").length
        jumlah += jmespath.search(daftar_butir_raw_data, "[? edited == `true`]").length
        jumlah += jmespath.search(daftar_butir_raw_data, "[? deleted == `true`]").length
        return jumlah;
        break
    }
}

daftar_validasi_raw_data = []
daftar_validasi_data = () => { return daftar_validasi_raw_data.filter((el) => el.invalid == true) }
daftar_validasi_invalid_arr = () => {
    // return $('#daftar-validasi-t').bootstrapTable('getAllSelections').map((el) => el.id)
}
sessionStorage.setItem('lasttime_commit_components', (new Date()).getTime()-100000)
sessionStorage.setItem('lasttime_commit_units', (new Date()).getTime()-100000)



// . Entry Table Uraian (Units)

function jumlahCalc(value, row) {
    let newval = row.volume * row.harga_satuan
    return '<em>' + $.number(newval, 2, ',', ' ') + '</em>'
}

function ribuanFormatter(value, row) {
    return $.number(value, 2, ',', ' ')
}

function countKomponenCalc(value, row) {
    return row.components.filter((el) => !el.hasOwnProperty('deleted')).length
}

function jumlah_FooterFormatter(data) {
    fraksi_progress.pembilang = data.reduce((acc, row) => acc + (row.harga_satuan*row.volume), 0)
    // render('persentase-progress')
    return $.number(fraksi_progress.pembilang, 2, ',', ' ')
}

function teksTotal_FooterFormatter(data) {
    return 'Total'
}

function nilaiTotalKomponen_FooterFormatter(data) {
    let nilaiTotalKomponen = data.reduce((acc, row) => acc + row.nilai_total_komponen, 0)
    return $.number(nilaiTotalKomponen, 2, ',', ' ')
}






// daftar validasi

$('#daftar-validasi-t').bootstrapTable({
    data: daftar_validasi_data()
})







// .  .                __         .
// |  |._. _.* _.._   /  ` _.._. _|
// |__|[  (_]|(_][ )  \__.(_][  (_]
                                

$(".card:has(#uraian-pekerjaan-fe) a:has(.fa-window-close)").on('click', (event) => {
    render('refresh-all-tables')
    $('.card:has(#uraian-pekerjaan-fe)').hide()
})

$('#daftar-uraian-pekerjaan-t').bootstrapTable({
    data: daftar_butir_data()
})
$('#daftar-uraian-pekerjaan-t').bootstrapTable('refreshOptions', {
    theadClasses: 'bg-purple text-white'
})
$('#daftar-uraian-pekerjaan-t').on('click-row.bs.table', function (row, $element, field) {
    states.add('uraian')
    states.delete('create')
    states.add('edit')
    render('status-entri-form-c', {})
    $('.card:has(#uraian-pekerjaan-fe)').fadeIn()
    $('#daftar-uraian-pekerjaan-t tr').each((index, el) => $(el).removeClass("table-active"))
    field.addClass('table-active')
    butir_terpilih_index = daftar_butir_raw_data.findIndex((el) => {
        return (el.id === $element.id)
    })
    $('#nomor-i').val($element.nomor)
    $('#kegiatan-i').val($element.kegiatan)
    $('#kode-analisa-i').val($element.kode_analisa)
    $('#volume-i').val($element.volume)
    $('#satuan-i').val($element.satuan)
    $('#harga-satuan-i').val($element.harga_satuan)
    $('#jumlah-harga-i').val(($element.harga_satuan * $element.volume))
    $('#kode-analisa-apply-btn').trigger('click')
})
$('.card:has(#daftar-uraian-pekerjaan-t) button.tambah').on('click', (event) => {
    states.add('uraian')
    states.delete('edit')
    states.add('create')
    render('status-entri-form-c', {})
    $('.card:has(#uraian-pekerjaan-fe)').fadeIn()
    $('#daftar-uraian-pekerjaan-t tr').each((index, el) => $(el).removeClass("table-active"))
    $('#uraian-pekerjaan-fe .form-control').each((index, el) => $(el).val(''))
    $('#nomor-i').trigger('focus')
})
$('#simpan-local-perubahan-butir-data').on('click', (event) => {
    let row = {
        'komponen_id': komponen_terpilih_id(),
        'id': '',
        'kegiatan': $('#kegiatan-i').val(),
        'kode_analisa': $('#kode-analisa-i').val(),
        'volume': $('#volume-i').val(),
        'satuan': $('#satuan-i').val(),
        'harga_satuan': $('#harga-satuan-i').val(),
        'nomor': $('#nomor-i').val(),
        'subnomor': 0
    }
    if (states.has('create')) {
        // create temp id with prefix "new-"
        row.id = `new-${daftar_komponen_raw_data.length}`
        daftar_butir_raw_data.push(row)
    }
    if (states.has('edit')) {
        row.edited = true
        row.id = daftar_butir_raw_data[butir_terpilih_index].id
        daftar_butir_raw_data[butir_terpilih_index] = row
    }
    render('refresh-all-tables')
    render('daftar-outline-l', daftar_grup_komponen_data())
    $('.card:has(#uraian-pekerjaan-fe)').hide()
    $('.card:has(#daftar-uraian-pekerjaan-t) button.tambah').trigger("focus")
})


$(".card:has(#uraian-pekerjaan-fe) a:has(.fa-trash)").on('click', (event) => {
    daftar_butir_raw_data[butir_terpilih_index].deleted = true
    render('refresh-all-tables')
    $('.card:has(#uraian-pekerjaan-fe)').hide()
})

// $('#simpan-perubahan-butir-raw-data').on('click', (event) => {
//     commitUnitsChanges(_token, daftar_butir_raw_data)
// })

$('#simpan-perubahan-butir-raw-data').magnificPopup({
    items: {
        src: `<div class="dialog">
                <p>Apakah anda yakin untuk menyimpan perubahan uraian ini?</p>
                <button type="button" 
                    class="btn btn-sm btn-primary dialog-dismiss is-accept" 
                    onclick="
                        commitUnitsChanges(_token, daftar_butir_raw_data);
                        handlerGetUnits(window.temp.$el, window.temp.event);
                                        render('persentase-progress');
                    ">Ya</button>
                <button type="button" class="btn btn-sm btn-secondary dialog-dismiss is-decline">Tidak</button>
            </div>`,
        type: 'inline'
    }
});



// . /~         |/  ,_     ,_ _ ,_
// . \_||`L||)  |\()||||)()||(/_||
// .        |          |            
// . Grup Komponen

$(".card:has(#grup-komponen-fe) a:has(.fa-window-close)").on('click', (event) => {
    render('refresh-all-tables')
    $('.card:has(#grup-komponen-fe)').hide()
})

$('#daftar-grup-komponen-t').bootstrapTable({
    data: daftar_grup_komponen_data()
})
$('#daftar-grup-komponen-t').bootstrapTable('refreshOptions', {
    theadClasses: 'bg-primary text-white'
})
$('#daftar-grup-komponen-t').on('click-row.bs.table', function (row, $element, field) {
    states.delete('komponen')
    states.add('grup-komponen')
    states.delete('create')
    states.add('edit')
    render('status-entri-form-c', {})
    $('.col:has(#daftar-komponen-t)').fadeIn()
    $('.card:has(#grup-komponen-fe)').fadeIn()
    $('.card:has(#grup-komponen-fe) > .card-header').attr('class', "card-header bg-primary")
    $('.card:has(#grup-komponen-fe) .card:last-child').fadeOut()
    $('#simpan-local-perubahan-komponen-data').fadeOut()
    $('#simpan-local-perubahan-grup-komponen-data').fadeIn()
    $('#daftar-grup-komponen-t tr').each((index, el) => $(el).removeClass("table-active"))
    $('#daftar-komponen-t tr').each((index, el) => $(el).removeClass("table-active"))
    field.addClass('table-active')
    // komponen_terpilih_index = field.index()
    komponen_terpilih_index = daftar_komponen_raw_data.findIndex((el) => {
        return (el.id === $element.id)
    })
    $('#daftar-komponen-t').bootstrapTable('load', daftar_komponen_terpilih_data())
    $('#urutan-romawi-i').val($element.urutan_romawi)
    $('#nama-grup-komponen-i').val($element.nama_grup_komponen)
})
$('.col:has(#daftar-grup-komponen-t) button.tambah').on('click', (event) => {
    states.delete('komponen')
    states.add('grup-komponen')
    states.delete('edit')
    states.add('create')
    render('status-entri-form-c', {})
    $('.card:has(#grup-komponen-fe)').fadeIn()
    $('.card:has(#grup-komponen-fe) > .card-header').attr('class', "card-header bg-primary")
    $('.card:has(#grup-komponen-fe) .card:last-child').fadeOut()
    $('#simpan-local-perubahan-komponen-data').fadeOut()
    $('#simpan-local-perubahan-grup-komponen-data').fadeIn()
    $('#daftar-grup-komponen-t tr').each((index, el) => $(el).removeClass("table-active"))
    $('#daftar-komponen-t tr').each((index, el) => $(el).removeClass("table-active"))
    $('#grup-komponen-fe input').each((index, el) => $(el).val(''))
})
$('#simpan-local-perubahan-grup-komponen-data').on('click', (event) => {
    if (states.has('create')) {
        // create temp id with prefix "new-"
        let row = {
            'RAB_id': detail_RAB_data.id,
            'id': `new-${daftar_komponen_raw_data.length}`,
            'urutan_romawi': $('#urutan-romawi-i').val(),
            'nama_grup_komponen': $('#nama-grup-komponen-i').val(),
            'components': []
        }
        daftar_komponen_raw_data.push(row)
    }
    if (states.has('edit')) {
        daftar_komponen_raw_data[komponen_terpilih_index].urutan_romawi = $('#urutan-romawi-i').val()
        daftar_komponen_raw_data[komponen_terpilih_index].nama_grup_komponen = $('#nama-grup-komponen-i').val()
        daftar_komponen_raw_data[komponen_terpilih_index].edited = true
    }
    render('refresh-all-tables')
    render('daftar-outline-l', daftar_grup_komponen_data())
    $('.card:has(#grup-komponen-fe)').fadeOut()
})

// . |/  ,_     ,_ _ ,_
// . |\()||||)()||(/_||
// .        |          
// . Komponen

$('#daftar-komponen-t').bootstrapTable({
    data: daftar_komponen_terpilih_data()
})
$('#daftar-komponen-t').bootstrapTable('refreshOptions', {
    theadClasses: 'bg-info text-white'
})
$('#daftar-komponen-t').on('click-row.bs.table', function (row, $element, field) {
    states.delete('grup-komponen')
    states.add('komponen')
    states.delete('create')
    states.add('edit')
    $('.card:has(#grup-komponen-fe) > .card-header').attr('class', "card-header bg-info")
    $('.card:has(#grup-komponen-fe) .card:last-child').fadeIn()
    $('#simpan-local-perubahan-komponen-data').fadeIn()
    $('#simpan-local-perubahan-grup-komponen-data').fadeOut()
    $('#daftar-komponen-t tr').each((index, el) => $(el).removeClass("table-active"))
    field.addClass('table-active')
    // subkomponen_terpilih_index = field.index()
    subkomponen_terpilih_index = daftar_komponen_raw_data[komponen_terpilih_index].components.findIndex((el) => {
        return (el.id === $element.id)
    })
    let komponen_terpilih_data = daftar_komponen_raw_data[komponen_terpilih_index]
    $('#urutan-romawi-i').val(komponen_terpilih_data.urutan_romawi)
    $('#nama-grup-komponen-i').val(komponen_terpilih_data.nama_grup_komponen)
    $('#urutan-abjad-i').val($element.urutan_abjad)
    $('#nama-komponen-i').val($element.nama_komponen)
    $('#nilai-total-komponen-i').val($element.nilai_total_komponen)
})
$('.col:has(#daftar-komponen-t) button.tambah').on('click', (event) => {
    states.delete('grup-komponen')
    states.add('komponen')
    states.delete('edit')
    states.add('create')
    render('status-entri-form-c', {})
    $('.card:has(#grup-komponen-fe)').fadeIn()
    $('.card:has(#grup-komponen-fe) > .card-header').attr('class', "card-header bg-primary")
    $('.card:has(#grup-komponen-fe) .card:last-child').fadeIn()
    $('#simpan-local-perubahan-komponen-data').fadeIn()
    $('#simpan-local-perubahan-grup-komponen-data').fadeOut()
    $('#daftar-komponen-t tr').each((index, el) => $(el).removeClass("table-active"))
    $('#grup-komponen-fe input').each((index, el) => $(el).val(''))
    $('#urutan-romawi-i').val(daftar_komponen_raw_data[komponen_terpilih_index].urutan_romawi)
    $('#nama-grup-komponen-i').val(daftar_komponen_raw_data[komponen_terpilih_index].nama_grup_komponen)

})
$('#simpan-local-perubahan-komponen-data').on('click', (event) => {
    if (states.has('create')) {
        // create temp id with prefix "new--"
        let row = {
            'RAB_id': detail_RAB_data.id,
            'id': `new--${daftar_komponen_raw_data[komponen_terpilih_index].components.length}`,
            'grup_komponen_id': daftar_komponen_raw_data[komponen_terpilih_index].id,
            'urutan_abjad': $('#urutan-abjad-i').val(),
            'nama_komponen': $('#nama-komponen-i').val(),
            'nilai_total_komponen': $('#nilai-total-komponen-i').val(),
            'units': []
        }
        daftar_komponen_raw_data[komponen_terpilih_index].components.push(row)
    }
    if (states.has('edit')) {
        let komponen_terpilih_data = daftar_komponen_raw_data[komponen_terpilih_index]
        let row = komponen_terpilih_data.components[subkomponen_terpilih_index]
        row.urutan_abjad = $('#urutan-abjad-i').val()
        row.nama_komponen = $('#nama-komponen-i').val()
        row.nilai_total_komponen = $('#nilai-total-komponen-i').val()
        row.edited = true
        let this_urutan_romawi = $('#urutan-romawi-i').val()
        // should change root??
        if (komponen_terpilih_data.urutan_romawi != this_urutan_romawi) {
            new_gk_index = daftar_komponen_raw_data.findIndex((el) => {
                return (el.urutan_romawi === this_urutan_romawi)
            })
            if (new_gk_index > -1) {
                daftar_komponen_raw_data[komponen_terpilih_index].components.splice(subkomponen_terpilih_index, 1)
                daftar_komponen_raw_data[new_gk_index].components.push(row)
            }
        } else {
            // no need to change
            daftar_komponen_raw_data[komponen_terpilih_index].components[subkomponen_terpilih_index] = row
        }
    }
    render('refresh-all-tables')
    render('daftar-outline-l', daftar_grup_komponen_data())
})

// . 
// . |_|       _  |/  ,_     ,_ _ ,_    |-      /~       
// . | |(||)L|_\  |\()||||)()||(/_||  (||_(|L|  \_||`L||)
// .      |              |                             | 
// .
// . Hapus komponen atau grup-komponen
$(".card:has(#grup-komponen-fe) a:has(.fa-trash)").on('click', (event) => {
    // if (states.has('edit')) {
    if (states.has('grup-komponen')) {
        states.delete('grup-komponen')
        daftar_komponen_raw_data[komponen_terpilih_index].deleted = true
    }
    if (states.has('komponen')) {
        states.delete('komponen')
        console.log('komponen')
        let komponen_terpilih_data = daftar_komponen_raw_data[komponen_terpilih_index]
        let row = komponen_terpilih_data.components[subkomponen_terpilih_index]
        row.deleted = true
        daftar_komponen_raw_data[komponen_terpilih_index].components[subkomponen_terpilih_index] = row
    }
    // }
    render('refresh-all-tables')
    $('.card:has(#grup-komponen-fe)').hide()
})




//  __.                 .__          .     .          .        __.                
// (__ *._ _ ._  _.._   [__) _ ._.. .|_  _.|_  _.._   ;_/ _   (__  _ ._..  , _ ._.
// .__)|[ | )[_)(_][ )  |   (/,[  (_|[_)(_][ )(_][ )  | \(/,  .__)(/,[   \/ (/,[  
//           |                                                                    

// $('#simpan-perubahan-komponen-raw-data').on('click', (event) => {
//     commitComponentsChanges(_token, daftar_komponen_raw_data)
// })

$('#simpan-perubahan-komponen-raw-data').magnificPopup({
    items: {
        src: `<div class="dialog">
                <p>Apakah anda yakin untuk menyimpan perubahan ini?</p>
                <button type="button" 
                    class="btn btn-sm btn-primary dialog-dismiss is-accept" 
                    onclick="
                        commitComponentsChanges(_token, daftar_komponen_raw_data);
                    ">Ya</button>
                <button type="button" class="btn btn-sm btn-secondary dialog-dismiss is-decline"
                    onclick="$.magnificPopup.close();">Tidak</button>
            </div>`,
        type: 'inline'
    }
});
// $('.dialog-dismiss').on('click', (e) => {
//     e.preventDefault();
//     $.magnificPopup.close();
// });
    

// ===============================================================================================

// .
// . |) _ ,_ | _     |-    |_|~|~|\/|| 
// . |\(/_||(|(/_|`  |_()  | | | |  ||_
// . 
// . Render to HTML

function render(template_name, data = {}) {
    let tpl = ""
    switch (template_name) {
        case 'daftar-outline-l':
            tpl = ""
            let i = 0
            for (const gk of data) {
                tpl += `<li cass="toc-h2">${convertToRoman(gk.urutan_romawi)}. ${gk.nama_grup_komponen}</li>`
                let j = 0
                for (const k of gk.components) {
                    tpl += `
                    <li class="list-group-item d-flex border-0 justify-content-between align-items-center ml-3 px-1 py-0">
                        <a href="#" 
                            data-komponen="${k.id}" 
                            data-index="[${i}, ${j}]" 
                            data-ids="[${gk.id}, ${k.id}]" 
                            onclick="getUnitsFromOutline(this, event)"
                        >
                            <i class="fa fa-edit" style="display:none;"></i> ${k.urutan_abjad}. ${k.nama_komponen}
                        </a>
                        <span class="badge badge-primary">
                            Rp ${ $.number(k.nilai_total_komponen,2) }
                        </span>
                    </li>`
                    j++
                }
                i++
            }
            let main_template = `<ul class="list-unstyled section-nav">${tpl}</ul>`
            $("#daftar-outline-l").html(main_template)
            break;
        case 'status-entri-form-c':
            tpl = ""
            tpl += (states.has('create')) ? `<em>(Tambah Baru)<em>` : ''
            tpl += (states.has('edit')) ? `<em>(Ubah)<em>` : ''
            $(".status").html(tpl)
            break;
        case 'title-sumber-uraian-pekerjaan':
            tpl = `<em>(${daftar_komponen_raw_data[komponen_terpilih_index].components[subkomponen_terpilih_index].nama_komponen})<em>`
            $(".sumber").html(tpl)
            break;
        case 'persentase-progress':
            // console.log(persentase_progress())
            $('#uraian_cards .percentages .text-muted').html(
                `Persentase dari Total/Target : (Rp ${fraksi_progress.pembilang})/(Rp ${fraksi_progress.pembagi})`)
            $('#uraian_cards .percentages strong').html(persentase_progress() + ' %')
            $('#uraian_cards .progress-bar').width(persentase_progress()+"%").attr('aria-valuenow', persentase_progress())
            break;
        case 'refresh-all-tables':
            $('#daftar-uraian-pekerjaan-t').bootstrapTable('load', daftar_butir_data())
            $('#daftar-grup-komponen-t').bootstrapTable('load', daftar_grup_komponen_data())
            $('#daftar-komponen-t').bootstrapTable('load', daftar_komponen_terpilih_data())
            break;
        case 'table-daftar-validasi':
            $('#daftar-validasi-t').bootstrapTable('load', daftar_validasi_data())
            break
        case 'detail-rab-overview-t':
            let l = $('#detail-rab-overview-t .right')
            for (let i = 0; i < l.length; i++) {
                const $element = $(l[i]);
                $element.text(detail_RAB_data[$element.attr('data-name')])
            }
            break;
    }
}

render('detail-rab-overview-t')
render('daftar-outline-l', daftar_grup_komponen_data())

function convertToRoman(num) {
    var roman = {
        M: 1000,
        CM: 900,
        D: 500,
        CD: 400,
        C: 100,
        XC: 90,
        L: 50,
        XL: 40,
        X: 10,
        IX: 9,
        V: 5,
        IV: 4,
        I: 1
    };
    var str = '';

    for (var i of Object.keys(roman)) {
        var q = Math.floor(num / roman[i]);
        num -= q * roman[i];
        str += i.repeat(q);
    }

    return str;
}



// .  .                    .___.   .     
// |\ | _..  ,* _  _. __*    |   _.|_  __
// | \|(_] \/ |(_](_]_) |    |  (_][_)_) 
//             ._|                       

// fix menu pills   
$('#menu-pills-tab a[href="#uraian_cards"]').tab('show')
$('#menu-pills-tab a[href="#komponen_cards"]').tab('show')

$('#menu-pills-tab a[href="#uraian_cards"]').on('shown.bs.tab', function (e) {
    if(jumlah_perubahan_pada('daftar_komponen_raw_data') == 0) {
        // $(e.target).trigger('click')
        $("#daftar-outline-l").attr('class', "card-body")
    } else {
       $.magnificPopup.open({   
            items: {
                src: `<div class="dialog">
                        <p>Ada perubahan pada tab <em>Entri Komponen</em> yang tidak tersimpan sebelumnya. Apakah anda sudah yakin dengan perubahan dan ingin menyimpan perubahan tersebut?</p>
                        <button type="button" 
                            class="btn btn-sm btn-primary dialog-dismiss is-accept" 
                            onclick="
                                commitComponentsChanges(_token, daftar_komponen_raw_data);
                            ">Ya, Simpan</button>
                        <button type="button" 
                            class="btn btn-sm btn-secondary dialog-dismiss is-decline"
                            onclick="
                                $.magnificPopup.close();
                                $('#menu-pills-tab a[href=\\'#komponen_cards\\']').trigger('click');
                            ">Tidak, kembali ke Tab Entri Komponen</button>
                    </div>`,
                type: 'inline'
            },
            modal: true
        });
    }
})

$("#daftar-outline-l").attr('class', "card-body no-click")

window.temp = {
    '$el': null,
    'event': null
}
function getUnitsFromOutline($el, event) {
    if(jumlah_perubahan_pada('daftar_butir_raw_data') == 0) { // memang tidak ada perubahan, 
        $('.card-body:has(#daftar-uraian-pekerjaan-t)').show();
        render('persentase-progress')
        handlerGetUnits($el, event);
    } else {
        window.temp.$el = $el
        window.temp.event = event
        $.magnificPopup.open({   
                    items: {
                        src: `<div class="dialog">
                                <p>Ada perubahan pada tab <em>Entri Uraian</em> yang tidak tersimpan sebelumnya. Apakah anda sudah yakin dengan perubahan dan ingin menyimpan perubahan tersebut?</p>
                                <button type="button" 
                                    class="btn btn-sm btn-primary dialog-dismiss is-accept" 
                                    onclick="
                                        $.magnificPopup.close();
                                        commitUnitsChanges(_token, daftar_butir_raw_data);
                                        handlerGetUnits(window.temp.$el, window.temp.event);
                                        render('persentase-progress');
                                    ">Ya, Simpan</button>
                                <button type="button" 
                                    class="btn btn-sm btn-secondary dialog-dismiss is-decline"
                                    onclick="
                                        $.magnificPopup.close();
                                    ">Tidak, kembali ke Tab Entri Komponen</button>
                            </div>`,
                        type: 'inline'
                    },
                    modal: true
                });
    }
}

$('#menu-pills-tab a[href="#komponen_cards"]').on('shown.bs.tab', function (e) {
    if(jumlah_perubahan_pada('daftar_butir_raw_data') == 0) {
        // $(e.target).trigger('click')
        $("#daftar-outline-l").attr('class', "card-body no-click")
    } else {
       $.magnificPopup.open({   
           items: {
                src: `<div class="dialog">
                        <p>Ada perubahan pada tab <em>Entri Uraian</em> yang tidak tersimpan sebelumnya. Apakah anda sudah yakin dengan perubahan dan ingin menyimpan perubahan tersebut?</p>
                        <button type="button" 
                            class="btn btn-sm btn-primary dialog-dismiss is-accept" 
                            onclick="
                                $.magnificPopup.close();
                                handlerGetUnits(window.temp.$el, window.temp.event);
                                render('persentase-progress');
                            ">Ya, Simpan</button>
                        <button type="button" 
                            class="btn btn-sm btn-secondary dialog-dismiss is-decline"
                            onclick="
                                $.magnificPopup.close();
                                $('#menu-pills-tab a[href=\\'#units_cards\\']').trigger('click');
                            ">Tidak, kembali ke Tab Entri Komponen</button>
                    </div>`,
                type: 'inline'
            },
            modal: true
        });
    }
})


// ===============================================================================================

// .__.               
// [__]  * _.\./ _  __
// |  |  |(_]/'\(/,_) 
//     ._|            

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

async function handlerGetUnits(self, e) {
    e.preventDefault();
    isLoading().loading()
    let komponen_id = $(self).attr("data-komponen");
    let data_ids = JSON.parse($(self).attr("data-ids"))
    // console.log(data_ids)
    // komponen_terpilih_index = JSON.parse($(self).attr("data-index"))[0]
    // subkomponen_terpilih_index = JSON.parse($(self).attr("data-index"))[1]
    komponen_terpilih_index = daftar_komponen_raw_data.findIndex((row) => row.id === data_ids[0])
    subkomponen_terpilih_index = daftar_komponen_raw_data[komponen_terpilih_index].components.findIndex((row) => row.id === data_ids[1])
    fraksi_progress.pembagi = daftar_komponen_raw_data[komponen_terpilih_index]
                                        .components[subkomponen_terpilih_index]
                                            .nilai_total_komponen
    const result = await $.ajax({
        url: _url['frontend.entry.RAB.sheet.units.commit'](komponen_id),
        type: "GET",
        datatype: 'json',
        data: {
            _token: _token,
        }
    })
    daftar_butir_raw_data = JSON.parse(result)
    $('.card-body:has(#daftar-uraian-pekerjaan-t) .alert').html('true')
    render('title-sumber-uraian-pekerjaan')
    render('refresh-all-tables')
    render('persentase-progress')
    isLoading().remove()
    return false;
}

async function commitComponentsChanges(_token, daftar_komponen_raw_data) {
    isLoading().loading()
    console.log((new Date()).getTime() - sessionStorage.getItem('lasttime_commit_components'))
    if((new Date()).getTime() - sessionStorage.getItem('lasttime_commit_components') > 10000) {
        sessionStorage.setItem('lasttime_commit_components', (new Date()).getTime())
        console.log('do')
        const result = await $.ajax({
            url: _url["frontend.entry.RAB.sheet.components.commit"],
            type: "POST",
            datatype: 'json',
            data: {
                _token: _token,
                daftar_komponen_raw: daftar_komponen_raw_data
            }
        })
        $('.card-body:has(#daftar-grup-komponen-t) .alert').html(result)
        isLoading().remove()
        window.location.reload(false)
    } else {
        console.log('dont')
        $('.card-body:has(#daftar-grup-komponen-t) .alert').html("aksi kirim entri komponen yang anda lakukan sudah dilakukan 10 detik sebelumnya, silahkan menunggu beberapa saat sebelum melakukan pengiriman entri kembali")
        isLoading().remove()
    }
}

async function commitUnitsChanges(_token, daftar_butir_raw_data) {
    $.magnificPopup.close();
    isLoading().loading()
    console.log((new Date()).getTime() - sessionStorage.getItem('lasttime_commit_units'))
    if((new Date()).getTime() - sessionStorage.getItem('lasttime_commit_units') > 10000) {
        sessionStorage.setItem('lasttime_commit_units', (new Date()).getTime())
        console.log('do')
        const result = await $.ajax({
            url: _url['frontend.entry.RAB.sheet.units.commit'](),
            type: "POST",
            datatype: 'json',
            data: {
                _token: _token,
                daftar_butir_raw: daftar_butir_raw_data
            }
        })
        $('.card-body:has(#daftar-uraian-pekerjaan-t) .alert').html(result)
        isLoading().remove()
    } else {
        console.log('dont')
        $('.card-body:has(#daftar-uraian-pekerjaan-t) .alert').html("aksi kirim entri unit yang anda lakukan sudah dilakukan 10 detik sebelumnya, silahkan menunggu beberapa saat sebelum melakukan pengiriman entri kembali")
        isLoading().remove()
    }
    // window.location.reload(false)
    // console.log(result)
    // console.log('push a notification that commits is done')
}

async function handlerGetValids(_token, rab_terpilih_id = detail_RAB_data.id) {
    const result = await $.ajax({
        url: _url['frontend.proses.valids.show'](rab_terpilih_id),
        type: "GET",
        datatype: 'json',
        data: {
            _token: _token,
        }
    })
    daftar_validasi_raw_data = JSON.parse(result)
    let response_text = daftar_validasi_data().length > 0 ? 
        `Pengunduhan berhasil, terdapat beberapa uraian yang perlu dicek kembali.` : 
        `Proses uraian berhasil, tidak ada uraian yang perlu dicek kembali`
    $(".card:has(#daftar-validasi-t) .alert").html(response_text)
    render('table-daftar-validasi')
    return false;
}

// });