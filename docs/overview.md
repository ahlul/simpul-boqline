---
title: Overview Aplikasi
date: '2020-04-24 03:18:38'
tags: #docs #aplikasi
---

# 202004240318 Overview Aplikasi BOQ-Metro

## SM Requirement

20200422 - 1872 - Rancangan Aplikasi Web Entri BOQ Metro

Disampaikan file yang berkaitan dengan BoQ, untuk kebutuhan pembuatan program entri BoQ.

Pada file "Form Entri BoQ Standar BPS" sudah saya buat sheet:
1. From Entri RAB, yang berisi format entri data RAB beserta sedikit contoh merujuk kepada file BoQ bangunan Bandar Lampung.
2. Database AHSP, sheet masih kosong, akan segera saya buat database nya sesuai dengan kode analisa standar.
3. Form Data BoQ, berisi file data BoQ yang merupakan gabungan data RAB dan Database AHSP. Pada sheet ini ada tambahan kolom sistem, yang dientri manual nantinya oleh admin, yang berfungsi sebagai kode untuk membuat tabel rekapitulasi.
4. Contoh Form BoQ, berdasarkan data dari Bangunan Bandar Lampung.
5. Gambar Exampel, berisi gambaran ttg proses penyatuan dua file menjadi 1 file Data BoQ.

Rancangan webentry harapannya terdapat menu:
1. Entri kuesioner (RAB), 
   menu untuk mengentri dokumen RAB
2. Download, 
   menu untuk setiap pihak mendownload file terkait dengan info hasil data Indeks Kemahalan Konstuksi (IKK), publikasi (IKK), dan informasi umum lainnya
3. Upload, 
   menu untuk stakeholder external (PU) untuk mengupload file data RAB dan file data pendukung lainnya (support all file, pdf word excell)
4. Informasi, 
   menu ini berisi tata cara panduan menggunakan aplikasi entri ini

Mohon bantuannya ya Karom, biaya dan keperluan yang dibutuhkan dalam proses pembuatan program ini tolong segera samwspaikan ke saya untuk segera saya penuhi.

Sebagai informasi awal juni saya sdh harus seminar hasil. Mohon maaf ya tergesa2 jadinya.

Note: 
RAB (Rancangan Anggaran Belanja)
AHSP (Analisa Harga Standar Pekerjaan)

## Arsitektur Sistem

```puml
@startwbs
!includeurl C:\Users\kjarom\.plantuml\style.pu

* Web Entri BOQ Metro
** Entri
*** RAB
*** AHSP
** Download
*** Data IKK
*** Publikasi IKK
** Upload
*** rawfile RAB
*** rawfile Lainnya
** Informasi
*** Tutorial Penggunaan Apps Web
@endwbs
```

## Rancangan Database

```puml
@startuml
!includeurl C:\Users\kjarom\.plantuml\style.pu

left to right direction
entity RAB {
    * id
    --
    * kegiatan_utama
    * pekerjaan
    * tahun
    * lokasi
    * jumlah_nilai_proyek (sebelum ppn 10%)
}

entity Komponen {
    * id
    * RAB_id
    --
    * grup_kegiatan (dipecah karena tidak fixed)
    * nomor
}

entity Butir {
    * id
    * komponen_id
    --
    * kegiatan
    * volume
    * nomor
    * subnomor
    * kode_analisa
      analisa_id
}

RAB }|..|| Komponen
Komponen }|..|| Butir

entity AHSP {
    * id
    --
    * dasar_hukum
    * tahun_anggaran
    * file_AHSP
}

entity Komoditas {
    * id
    * AHSP_id
    --
    * komoditas
    * kelompok_komoditas
    * kode_analisa
}

entity Uraian {
    * id
    * komoditas_id
    --
    * uraian_csv
    * nilai_akhir
}

AHSP }|..|| Komoditas
Komoditas }|..|| Uraian
@enduml
```

## Business Process

Manajemen User
```puml
@startuml
!includeurl C:\Users\kjarom\.plantuml\style.pu
start;
:user login;
if (apakah admin?) then (ya)
    :manajemen pengguna;
    stop
else (tidak)
    stop;
endif
@enduml
```

Business Prosess Utama akan dibahas lebih lanjut

## Use Case

Use case akan dibahas lebih lanjut

## Spesifikasi Sistem

Spesifikasi sistem (hardware dan software) akan dibahas lebih lanjut
Server Hosting : https://www.flazznetworks.com/personal-hosting/