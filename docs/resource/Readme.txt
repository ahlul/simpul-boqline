Assalamualaikum Wr. Wb.

Disampaikan file yang berkaitan dengan BoQ, untuk kebutuhan pembuatan program entri BoQ.

Pada file "Form Entri BoQ Standar BPS" sudah saya buat sheet:
(1) From Entri RAB, yang berisi format entri data RAB beserta sedikit contoh merujuk kepada file BoQ bangunan Bandar Lampung.
(2) Database AHSP, sheet masih kosong, akan segera saya buat database nya sesuai dengan kode analisa standar.
(3) Form Data BoQ, berisi file data BoQ yang merupakan gabungan data RAB dan Database AHSP. Pada sheet ini ada tambahan kolom sistem, yang dientri manual nantinya oleh admin, yang berfungsi sebagai kode untuk membuat tabel rekapitulasi.
(4) Contoh Form BoQ, berdasarkan data dari Bangunan Bandar Lampung.
(5) Gambar Exampel, berisi gambaran ttg proses penyatuan dua file menjadi 1 file Data BoQ.

Rancangan webentry harapannya terdapat menu:
(1) Entri kuesioner (RAB), menu untuk mengentri dokumen RAB
(2) Download, menu untuk setiap pihak mendownload file terkait dengan info hasil data Indeks Kemahalan Konstuksi (IKK), publikasi (IKK), dan informasi umum lainnya
(3) Upload, menu untuk stakeholder external (PU) untuk mengupload file data RAB dan file data pendukung lainnya (support all file, pdf word excell)
(4) Informasi, menu ini berisi tata cara panduan menggunakan aplikasi entri ini

Mohon bantuannya ya Karom, biaya dan keperluan yang dibutuhkan dalam proses pembuatan program ini tolong segera sampaikan ke saya untuk segera saya penuhi.

Sebagai informasi awal juni saya sdh harus seminar hasil. Mohon maaf ya tergesa2 jadinya.

Demikian.

Best Regards,

---------------------------------
Ade Fitriansyah, S.S.T., M.S.E.
Kepala Seksi Statistik Distribusi
BPS Kota Metro Provinsi Lampung
---------------------------------
