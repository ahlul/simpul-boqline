
use case
```puml
@startuml
!includeurl C:\Users\kjarom\.plantuml\style.pu

left to right direction
user << Human >>
admin << System >>
(Melihat isian RAB) as (Usecase 2)
(Mengubah isian RAB) as (Usecase 1)
(Mengelola pengguna) as (Usecase 3)
(Mengunduh RAB)

user --> (Usecase 1) 
user --> (Usecase 2)
(Usecase 2) <-- admin
(Usecase 3) <-- admin

@enduml
```

<!-- menu aplikasi
```puml
@startwbs
!includeurl C:\Users\kjarom\.plantuml\style.pu

* Business Process Modelling WBS
** Launch the project
*** Complete Stakeholder Research
*** Initial Implementation Plan
** Design phase
*** Model of AsIs Processes Completed
**** Model of AsIs Processes Completed1
**** Model of AsIs Processes Completed2
*** Measure AsIs performance metrics
*** Identify Quick Wins
** Complete innovate phase
@endwbs
``` -->

