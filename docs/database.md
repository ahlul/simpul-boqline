database
```puml
@startuml
!includeurl C:\Users\kjarom\.plantuml\style.pu

left to right direction
entity RAB {
    * id
    --
    * kegiatan_utama
    * pekerjaan
    * tahun
    lokasi
}

entity Komponen {
    * id
    * RAB_id
    --
    * grup_kegiatan (dipecah karena tidak fixed)
    * nomor
}

entity Butir {
    * id
    * komponen_id
    --
    * kegiatan
    * volume
    * nomor
    * subnomor
    * kode_analisa
      analisa_id
}

RAB }|..|| Komponen
Komponen }|..|| Butir

entity AHSP {
    * id
    --
    * dasar_hukum
    * tahun_anggaran
    * file_AHSP
}

entity Komoditas {
    * id
    * AHSP_id
    --
    * komoditas
    * kelompok_komoditas
    * kode_analisa
}

entity Uraian {
    * id
    * komoditas_id
    --
    * uraian_csv (harus dipecah)
    * nilai_akhir
}

AHSP }|..|| Komoditas
Komoditas }|..|| Uraian
@enduml
```



<!-- Entity01 }|..|| Entity02
Entity03 }o..o| Entity04
Entity05 ||--o{ Entity06
Entity07 |o--|| Entity08 -->