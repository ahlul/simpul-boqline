---
title: Hasil Pertemuan Kedua Sistem Entri BOQ Online
date: '2020-05-10 20:05:16'
tags: #desain #sistem #pemrogramman
---

# 202005102005 Hasil Pertemuan Kedua Sistem Entri BOQ Online

Abbr:
BP  : Business Process
SM  : Subject Matter
SD  : System Developer

## Business Prosess Utama
BP ini merupakan BP Gabungan dari proses entri hingga menjadi Laporan yang akan digunakan oleh SM.

```puml
@startuml
|Operator|
start
while (data sudah selesai di entri) is (no)
    :entri RAB;
    :simpan hasil entri;
endwhile (yes)
:kirim RAB;

|#GhostWhite|Admin|
:notifikasi RAB;
:validasi RAB;
if (validasi RAB sesuai) then (yes)
    :gabung dengan AHSP;
    :entri kode sistem;
    :generate hasil BOQ;
    end
else (no)
    :kirim kembali ke operator;
    detach
endif

@enduml
```

## Desain Database

```puml
@startuml
!includeurl C:\Users\kjarom\.plantuml\style.pu

left to right direction
entity RAB {
    * id
    --
    * kegiatan_utama
    * pekerjaan
    * tahun
    * lokasi
    * jumlah_nilai_proyek
}

entity GrupKomponen { //ComponentGroup
    * id
    * RAB_id
    --
    nama_grup_komponen
    urutan_romawi (I..V..)
}

entity Komponen {//Component
    * id
    * grup_komponen_id
    * nama_komponen
    * urutan_abjad (A..Z or 0 (kalau sama dengan komponen))
    nilai_total_komponen (calc)
}

entity Butir { /Unit
    * id
    * komponen_id
    --
    * kegiatan
    * volume
      satuan
      harga_satuan
      jumlah_harga (calc volume*harga_satuan) 
    * nomor
    * subnomor
    * kode_analisa
      analisa_id
}

RAB }|..|| GrupKomponen
GrupKomponen }|..|| Komponen
Komponen }|..|| Butir

entity AHSP { /AHSP
    * id
    --
    * dasar_hukum
    * tahun_anggaran
    * file_AHSP
}

entity Komoditas { /Commodity
    * id
    * AHSP_id
    --
    kode_analisa
    komponen_pekerjaan
    komoditi
    uraian
    kode_uraian
    satuan
    koefisien
    harga (harga satuan)
    nilai
}

AHSP }|..|| Komoditas
@enduml
```

## Breakdown Perubahan

- File yang akan digunakan sebagai percontohan adalah file **RAB Kota Metro**
- aplikasi entri akan lebih menekankan pada business prosess diatas
- terdapat perubahan yang signifikan pada perancangan database yakni:
  - penyederhanaan tabel Komoditas dan relasinya
  - adanya pembentukan tabel grupKomponen
  - penambahan kolom jumlah_nilai_proyek yang merupakan besaran total proyek sebelum dikurangi PPN 10%
- Aktor pada sistem ini ada dua yakni: Operator dan Admin
  - Operator bertugas mengentri data RAB dan mengupload data tambahan RAB tersebut
  - Admin bertugas melakukan validasi, penggabungan, serta penambahan kode sistem pada RAB sebelum pada akhirnya dihasilkan output gabungan antara RAB dan AHSP (BOQ)
- Kode Sistem merupakan konsep pengklasifikasiaan kegiatan kerja milik BPS
- progress RAB selesai di entri atau belum terlihat dari jumlah_nilai_proyek dikurang perjumlahan nilai dari tiap-tiap komoditas
- terdapat peruahan arsitektur sistem menjadi:
  - Entri : sistem pengentrian dan validasi RAB
  - Proses : terdapat menu proses validasi, generate BOQ dari RAB dan AHSP, serta mengunduh hasil generate BOQ
  - Upload : menu upload data-data terkait RAB
  - Informasi : memiliki menu unduhan IKK dan publikasi serta panduan penggunaan aplikasi

## Timeline Kegiatan Pengerjaan

| Tanggal   | Kegiatan                                               |
| --------- | ------------------------------------------------------ |
| 2020-5-16 | Subsistem pengentrian RAB selesai                      |
| 2020-5-23 | Susbsitem penggabunan RAB dan BOQ selesai              |
| 2020-5-30 | Aplikasi dapat diluncurkan dan diakses melalui browser |

## Rancangan Tampilan 

`menyusul`

## Catatan

Bahwa peluncuran aplikasi nantinya akan dikerjakan oleh SD

