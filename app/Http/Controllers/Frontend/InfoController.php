<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Document;

class InfoController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $ikk = Document::where("keterangan_dokumen", "Publikasi IKK")->get();
        return view('frontend.info', compact("ikk"));
    }
}
