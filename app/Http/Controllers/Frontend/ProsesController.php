<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RAB;
use App\Models\RAB\ComponentGroup;
use App\Models\RAB\Component;
use App\Models\RAB\Unit;
use App\Models\AHSP;
use App\Models\AHSP\Commodity;
use Illuminate\Support\Facades\DB;

class ProsesController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $daftar_rab = RAB::all();
        $ahsp = AHSP::first()->commodities->groupBy('kode_analisa');
        return view('frontend.proses', compact('daftar_rab', 'ahsp'));
    }

    public function showValids($id, Request $request) 
    {
        // validasi sequence
        $rab = RAB::find($id);

        // dapetin dulu seluruh id komponen
        $daftar_komponen = $rab->daftar_komponen;
        $daftar_komponen_ids = $daftar_komponen->sortBy('urutan_romawi')
                                    ->map(function ($gk) {
                                        return $gk->components->map(function ($k) {
                                            return $k->id;
                                        });
                                    })->flatten(1);
        // dd($daftar_komponen_ids->toArray());

        // trus baru dapetin daftar_validasi (bottom2top)
        $arr_invalid = json_decode($rab->notvalids ,true) ?? array();
        $daftar_validasi = Unit::whereIn(
                                'komponen_id', $daftar_komponen_ids->toArray()
                            )->get()->map(function ($u) use ($arr_invalid) {
                                $u->urutan_abjad = $u->component->urutan_abjad;
                                $u->urutan_romawi = $u->component->grup->urutan_romawi;
                                $c = Commodity::where('kode_analisa', $u->kode_analisa)
                                                        ->where('tipe_komoditi', 6)
                                                        ->first();
                                $is_ls = $u->kode_analisa == 'Ls' ? "Ls" : "";
                                $u->invalid = in_array($u->id, $arr_invalid);
                                $u->komponen_pekerjaan = $c->komponen_pekerjaan ?? $is_ls;
                                $u->komponen_harga_satuan_pekerjaan = $c->nilai ?? $is_ls;
                                return collect($u)->except(['component']);
                            });
        return $daftar_validasi->toJSON();
    }

    public function updateValids($id, Request $request)
    {
        $myrab = RAB::find($id);
        $myrab->notvalids = $request->notvalids;
        $myrab->save();

        return $myrab->toJSON();
    }

    public function showSystemCode($id, Request $request)
    {
        // validasi sequence
        $rab = RAB::find($id);

        // dapetin dulu seluruh id komponen
        $daftar_komponen = $rab->daftar_komponen;
        $daftar_komponen_ids = $daftar_komponen->sortBy('urutan_romawi')
                                    ->map(function ($gk) {
                                        return $gk->components->map(function ($k) {
                                            return $k->id;
                                        });
                                    })->flatten(1);
        // dd($daftar_komponen_ids->toArray());

        // trus baru dapetin daftar_validasi (bottom2top)
        $daftar_kodesistem = Component::find(
                                    $daftar_komponen_ids->toArray()
                                )->map(function ($u) {
                                    // $u->urutan_abjad = $u->component->urutan_abjad;
                                    $u->urutan_romawi = $u->grup->urutan_romawi;
                                    $u->nama_grup_komponen = $u->grup->nama_grup_komponen;
                                    return collect($u)->except(['grup']);
                                });
        // dd($daftar_kodesistem);
        return $daftar_kodesistem->toJSON();
    }

    public function updateSystemCode($id, Request $request)
    {
        $r_syscodes = collect(json_decode($request->syscodes));
        $my_syscodes_arr = $r_syscodes->groupBy('kode_sistem')
                        ->map(function ($c) {
                            return collect($c)->pluck('id');
                        })->toArray();
        // dd($my_syscodes_arr);

        foreach ($my_syscodes_arr as $kode_sistem=>$ids) {
            // dd($kode_sistem);
            $my_comp = Component::whereIn('id', $ids)->update(['kode_sistem' => $kode_sistem === "" ? null : $kode_sistem]);
        }

        return [ 'success' => true ];
    }

    public function showBOQ($id, Request $request)
    {
        // validasi sequence
        $rab = RAB::find($id);

        // dapetin dulu seluruh id komponen
        $daftar_komponen = $rab->daftar_komponen;
        // $daftar_komponen_ids = $daftar_komponen->sortBy('urutan_romawi')
        //                             ->map(function ($gk) {
        //                                 return $gk->components->map(function ($k) {
        //                                     return $k->id;
        //                                 });
        //                             })->flatten(1);
        // dd($daftar_komponen_ids->toArray());

        // trus baru dapetin daftar_validasi (bottom2top)
        // $boq = Unit::whereIn(
        //                 'komponen_id', $daftar_komponen_ids->toArray()
        //             )->get()->map(function ($u) {
        //                 $u->urutan_abjad = $u->component->urutan_abjad;
        //                 $u->urutan_romawi = $u->component->grup->urutan_romawi;
        //                 $u->komoditas = Commodity::where('kode_analisa', $u->kode_analisa)
        //                                         ->where('tipe_komoditi', 6)
        //                                         ->get();
        //                 return collect($u)->except(['component']);
        //             });
        $boq = $daftar_komponen->sortBy('urutan_romawi')
                                    ->map(function ($gk) {
                                        $gk->components->map(function ($k) {
                                            $k->units;
                                            return $k;
                                        });
                                        return $gk;
                                    })->flatten(1);
        // dd($boq->toArray());
        return $boq->toJSON();
    }

    public function storeBOQ($id, Request $request)
    {
        //
    }
}
