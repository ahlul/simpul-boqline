<?php

namespace App\Http\Controllers\Frontend\Entry\RAB;

use App\Http\Controllers\Controller;
use App\Models\RAB;
use App\Models\RAB\ComponentGroup;
use App\Models\RAB\Component;
use App\Models\RAB\Unit;
use App\Models\AHSP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class RABController extends Controller
{
    public function sheet($id)
    {
        // Default AHSP
        $daftar_kode_analisa = AHSP::find(1)->daftar_kode_analisa;
        
        // RAB terpilih
        $rab = RAB::find($id);
        $daftar_komponen = $rab->daftar_komponen;

        if ($daftar_komponen->isEmpty()) {
            return view('frontend.entry.RAB.sheet', compact('daftar_kode_analisa', 'rab'));
        }
        $komponen = $daftar_komponen->sortBy('urutan_romawi')->first()->components;

        if ($komponen->isEmpty()) {
            return view('frontend.entry.RAB.sheet', compact('daftar_kode_analisa', 'rab', 'daftar_komponen'));
        }
        $daftar_butir = $komponen->sortBy('urutan_abjad')->first()->units;

        return view('frontend.entry.RAB.sheet', compact('daftar_kode_analisa', 'rab', 'daftar_komponen', 'daftar_butir'));
    }

    public function showUnits($id, $num)
    {
        $daftar_butir = Component::find($num)->units;
        return $daftar_butir->toJSON();
    }

    public function commitComponents($id, Request $request)
    {
        $daftar_komponen_raw = collect($request->daftar_komponen_raw);
        $myrab = RAB::find($id);
        if($myrab->did_submit == true) {
            return "perubahan komponen tidak dilakukan karena telah dikunci";
        }

        // filter deleted:true when not 'new-*'
        $this->deleteComponents($id, $daftar_komponen_raw);
        // filter 'new-*'
        $this->storeComponents($id, $daftar_komponen_raw);
        // filter edited:true
        $this->updateComponents($id, $daftar_komponen_raw);
        return 'berhasil';
    }

    private function storeComponents(&$id, &$daftar_komponen_raw)
    {
        $myrab = RAB::find($id);
        $new_componentgroups_arr = $daftar_komponen_raw->filter(function ($gk) {
                                        return Str::is('new-*', $gk['id']);
                                    })->map(function ($gk) use ($id) {
                                        return collect($gk)->put('RAB_id', $id)->only(['RAB_id', 'urutan_romawi', 'nama_grup_komponen']);
                                    })->toArray();
        $new_componentgroups_arr = array_values($new_componentgroups_arr);
        $my_componentgroups = $myrab->group_components()->createMany($new_componentgroups_arr);
        // dd($my_componentgroups->toArray());

        $new_components_arr = $daftar_komponen_raw->filter(function($gk) {
                                return ( Arr::has($gk, 'components') and count($gk['components'])>0 );
                            })->map(function($gk) {
                                return $gk['components']; 
                            })->flatten(1)->filter(function ($k) {
                                return Str::is('new--*', $k['id']);
                            })->map(function ($k) use ($daftar_komponen_raw, $my_componentgroups) {
                                if ( !Str::is('new-*', $k['grup_komponen_id']) ) {
                                    return $k;
                                }
                                // find matched grup_komponen_id
                                $idx = $daftar_komponen_raw->search(function ($item, $key) use ($k) {
                                        return $item['id'] == $k['grup_komponen_id'];
                                    });
                                $j = $my_componentgroups->filter(function($cg) use ($daftar_komponen_raw, $idx) {
                                        return $cg['urutan_romawi'] == $daftar_komponen_raw[$idx]['urutan_romawi'];
                                    })->first();
                                // dd($k);
                                $k['grup_komponen_id'] = $j['id'];
                                // dd($k);
                                return $k;
                            })->map(function ($k) {
                                return collect($k)->except(['id', 'RAB_id']);
                            })->toArray();
        $new_components_arr = array_values($new_components_arr);
        $my_components = Component::insert($new_components_arr);
    }

    private function updateComponents(&$id, &$daftar_komponen_raw)
    {
        $myrab = RAB::find($id);
        $edited_componentgroups_arr = $daftar_komponen_raw->filter(function ($gk) {
                                        return (Arr::has($gk, 'edited') and !Arr::has($gk, 'deleted') and !Str::is('new-*', $gk['id']));
                                    })->map(function ($gk) use ($id) {
                                        return collect($gk)->put('RAB_id', $id)->except(['components']);
                                    })->toArray();
        $edited_componentgroups_arr = array_values($edited_componentgroups_arr);
        
        $edited_components_arr = $daftar_komponen_raw->filter(function($gk) {
                                return ( Arr::has($gk, 'components') and count($gk['components'])>0 );
                            })->map(function($gk) {
                                return $gk['components']; 
                            })->flatten(1)->filter(function ($k) {
                                return (Arr::has($k, 'edited') and !Arr::has($k, 'deleted') and !Str::is('new--*', $k['id']));
                            })->toArray();
        $edited_components_arr = array_values($edited_components_arr);

        DB::transaction(function () use ($edited_componentgroups_arr, $edited_components_arr) {
            foreach ($edited_componentgroups_arr as $arr) {
                ComponentGroup::updateOrCreate(
                    ['id' => $arr['id']],
                    [
                        'nama_grup_komponen' => $arr['nama_grup_komponen'], 
                        'urutan_romawi' => $arr['urutan_romawi']
                    ]
                );
            }
            foreach ($edited_components_arr as $arr) {
                Component::updateOrCreate(
                    ['id' => $arr['id']],
                    [
                        'grup_komponen_id' => $arr['grup_komponen_id'], 
                        'nama_komponen' => $arr['nama_komponen'], 
                        'urutan_abjad' => $arr['urutan_abjad'],
                        'nilai_total_komponen' => $arr['nilai_total_komponen']
                    ]
                );
            }
        }, 5);
    }

    private function deleteComponents(&$id, &$daftar_komponen_raw)
    {
        $myrab = RAB::find($id);
        $marked_componentgroups_arr = $daftar_komponen_raw->filter(function ($gk) {
                                        return (Arr::has($gk, 'deleted') and !Str::is('new-*', $gk['id']));
                                    })->map(function ($gk) use ($id) {
                                        return collect($gk)->only(['id']);
                                    })->flatten()->toArray();
        $marked_componentgroups_arr = array_values($marked_componentgroups_arr);
        $my_removedcomponentgroups_rows = ComponentGroup::whereIn('id', $marked_componentgroups_arr)->delete();

        $marked_components_arr = $daftar_komponen_raw->filter(function($gk) {
                                return ( Arr::has($gk, 'components') and count($gk['components'])>0 );
                            })->map(function($gk) {
                                return $gk['components']; 
                            })->flatten(1)->filter(function ($k) {
                                return (Arr::has($k, 'deleted') and !Str::is('new-*', $k['id']));
                            })->map(function ($k) {
                                return $k['id'];
                            })->toArray();
        $marked_components_arr = array_values($marked_components_arr);
        $my_removedcomponentgroups_rows = Component::whereIn('id', $marked_components_arr)->delete();
    }

    public function commitUnits($id, $num, Request $request)
    {
        $daftar_butir_raw = collect($request->daftar_butir_raw);
        $myrab = RAB::find($id);
        if($myrab->did_submit == true) {
            return "perubahan butir tidak dilakukan karena telah dikunci";
        }
        // dd($daftar_butir_raw);
        
        // filter deleted:true when not 'new-*'
        // $this->deleteComponents($id, $daftar_butir_raw);
        $mycomponent = RAB::find($num);
        $marked_units_arr = $daftar_butir_raw->filter(function ($b) {
                                        return (Arr::has($b, 'deleted') and !Str::is('new-*', $b['id']));
                                    })->map(function ($b) use ($id) {
                                        return collect($b)->only(['id']);
                                    })->flatten()->toArray();
        $marked_units_arr = array_values($marked_units_arr);
        // dd($marked_units_arr);
        $my_removedcomponentgroups_rows = Unit::whereIn('id', $marked_units_arr)->delete();

        // filter 'new-*'
        // $this->storeUnits($num, $daftar_butir_raw);
        $mycomponent = Component::find($num);
        $new_units_arr = $daftar_butir_raw->filter(function ($b) {
                                        return Str::is('new-*', $b['id']);
                                    })->map(function ($b) use ($id) {
                                        return collect($b)->except(['id', 'komponen_id']);
                                    })->toArray();
        $new_units_arr = array_values($new_units_arr);
        $my_units = $mycomponent->units()->createMany($new_units_arr);

        // filter edited:true
        // $this->updateComponents($id, $daftar_butir_raw);
        $mycomponent = Component::find($num);
        $edited_units_arr = $daftar_butir_raw->filter(function ($b) {
                                return (Arr::has($b, 'edited') and !Arr::has($b, 'deleted') and !Str::is('new-*', $b['id']));
                            })->toArray();
        $edited_units_arr = array_values($edited_units_arr);
        DB::transaction(function () use ($edited_units_arr) {
            foreach ($edited_units_arr as $arr) {
                Unit::updateOrCreate(
                    ['id' => $arr['id']],
                    [
                        'komponen_id' => $arr["komponen_id"],
                        'kegiatan' => $arr["kegiatan"],
                        'kode_analisa' => $arr["kode_analisa"],
                        'volume' => $arr["volume"],
                        'satuan' => $arr["satuan"],
                        'harga_satuan' => $arr["harga_satuan"],
                        'nomor' => $arr["nomor"],
                        'subnomor' => $arr["subnomor"],
                    ]
                );
            }
        }, 5);
        
        return 'berhasil';
    }

    // =============================================================

    public function createDetail()
    {
        $rab = new RAB;
        return view('frontend.entry.RAB.detail', compact(['rab']));
    }

    public function storeDetail(Request $request)
    {
        $myrab = new RAB;
        $myrab->kegiatan_utama = $request->kegiatan_utama;
        $myrab->pekerjaan = $request->pekerjaan;
        $myrab->tahun = $request->tahun;
        $myrab->lokasi = $request->lokasi;
        $myrab->jumlah_nilai_proyek = $request->jumlah_nilai_proyek;
        $myrab->save();
        return redirect()->route('frontend.entry.index');
    }

    public function showdetail($id)
    {
        //  readonly  
    }

    public function editdetail($id)
    {
        $data = RAB::find($id);
        return view('frontend.entry.RAB.detail', ["rab" => $data]);
    }

    public function updatedetail($id, Request $request)
    {
        $myrab = RAB::find($id);
        if($myrab->did_submit == true) {
            return redirect()->route('frontend.entry.index');
            // return "perubahan detail RAB tidak dilakukan karena telah dikunci"
        }
        $myrab->kegiatan_utama = $request->kegiatan_utama;
        $myrab->pekerjaan = $request->pekerjaan;
        $myrab->tahun = $request->tahun;
        $myrab->lokasi = $request->lokasi;
        $myrab->jumlah_nilai_proyek = $request->jumlah_nilai_proyek;
        $myrab->save();
        return redirect()->route('frontend.entry.index');
    }

    public function deletedetail($id)
    {
        $myrab = RAB::destroy($id);
        if($myrab->did_submit == true) {
            return redirect()->route('frontend.entry.index');
        }
        return redirect()->route('frontend.entry.index');
    }

    // =============================================================

    public function send($id) 
    {
        $myrab = RAB::find($id);
        $myrab->did_submit = true;
        $myrab->save();
        return redirect()->route('frontend.entry.index');
    }

    public function unsend($id) 
    {
        $myrab = RAB::find($id);
        $myrab->did_submit = false;
        $myrab->save();
        return redirect()->route('frontend.entry.index');
    }
}
