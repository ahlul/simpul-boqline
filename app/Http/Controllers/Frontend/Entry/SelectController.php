<?php

namespace App\Http\Controllers\Frontend\Entry;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RAB;

class SelectController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function __invoke()
    {   
        $data = RAB::all();
        // $data = $data->map(function ($rab) {
        //                     $rab->did_submit_text = $rab->did_submit ? 'Telah Dikirim' : 'Belum Dikirim';
        //                     return $rab;
        //                 });
        return view('frontend.entry.index', ['RAB' => $data]);
    }
}
