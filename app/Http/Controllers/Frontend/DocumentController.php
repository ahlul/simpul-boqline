<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Document;

class DocumentController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $docs = Document::get();
        return view('frontend.document', compact('docs'));
    }

    public function store(Request $request)
    {
        // $path = $request->file('dokumen')->store('dokumen');
        // $file = $request->file('dokumen');
        // $filename = $file->getClientOriginalName() . '_' . time() . '.' . $file->getClientOriginalExtension();
        // $path = $file->storeAs('documents', $filename);

        // dd($request);
        $about  = $request->keterangan_dokumen;
        $docs   = $request->file('dokumen');
        $paths  = [];
    
        foreach ($docs as $doc) {
            $extension = $doc->getClientOriginalExtension();
            $oriname   = $doc->getClientOriginalName(); 
            $filename  = $oriname . time() . '.' . $extension;
            $paths[]   = $doc->storeAs('public/documents', $filename);
        }
    
        // dd($paths);
        foreach($paths as $path) {
            $dokumen_ku = new Document;
            $dokumen_ku->keterangan_dokumen = $about;
            $dokumen_ku->lokasi = $path;
            $dokumen_ku->save();
        }

        // dd($paths);
        return redirect()->route('frontend.upload.index');
    }
}
