<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = "dokumen";
    protected $fillable = ["keterangan_dokumen", "lokasi"];
}
