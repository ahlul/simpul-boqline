<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AHSP extends Model
{
    protected $table = 'ahsp';
    protected $hidden = ['created_at', 'updated_at'];

    public function commodities()
    {
        return $this->hasMany('App\Models\AHSP\Commodity', 'AHSP_id');
    }

    public function getDaftarKodeAnalisaAttribute()
    {
        $ahsp_id = $this->id;
        return AHSP::find($ahsp_id)->commodities()
                                       ->select(['kode_analisa', 'komponen_pekerjaan', 'nilai'])
                                       ->where('tipe_komoditi', 6)
                                       ->distinct()
                                       ->getResults();
    }
}
