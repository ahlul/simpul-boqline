<?php

namespace App\Models\AHSP;

use Illuminate\Database\Eloquent\Model;

class Commodity extends Model
{
    protected $table = 'komoditas';
    protected $hidden = ['created_at', 'updated_at'];
}
