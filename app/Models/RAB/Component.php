<?php

namespace App\Models\RAB;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    protected $table = 'komponen';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['grup_komponen_id', 'urutan_abjad', 'nama_komponen', 'nilai_total_komponen'];

    public function units()
    {
        // RAB::find($this->id)
        return $this->hasMany('App\Models\RAB\Unit', 'komponen_id');
    }

    public function grup()
    {
        return $this->belongsTo('App\Models\RAB\ComponentGroup', 'grup_komponen_id');
    }
}
