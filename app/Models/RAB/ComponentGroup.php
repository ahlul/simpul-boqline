<?php

namespace App\Models\RAB;

use Illuminate\Database\Eloquent\Model;

class ComponentGroup extends Model
{
    protected $table = 'grupkomponen';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['RAB_id', 'urutan_romawi', 'nama_grup_komponen'];


    public function components()
    {
        // RAB::find($this->id)
        return $this->hasMany('App\Models\RAB\Component', 'grup_komponen_id');
    }
}
