<?php

namespace App\Models\RAB;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'butir';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = [
        "komponen_id",
        "kegiatan",
        "kode_analisa","volume","satuan","harga_satuan",
        "nomor","subnomor"
    ];

    public function component()
    {
        return $this->belongsTo('App\Models\RAB\Component', 'komponen_id');
    }
}
