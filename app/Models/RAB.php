<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RAB extends Model
{
    protected $table = 'rab';
    protected $hidden = ['created_at', 'updated_at'];
    protected $appends = ['progress'];

    public function getProgressAttribute()
    {
        // TODO: dynamic -__-
        return 20;
    }

    public function group_components()
    {
        return $this->hasMany('App\Models\RAB\ComponentGroup', 'RAB_id');
    }

    public function getDaftarKomponenAttribute()
    {
        return RAB::with('group_components.components')->find($this->id)->group_components;
    }
}
