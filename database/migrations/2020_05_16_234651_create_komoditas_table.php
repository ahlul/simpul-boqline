<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomoditasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Komoditas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('AHSP_id')->unsigned();
            $table->string('kode_analisa',10);
            $table->string('komponen_pekerjaan',100);
            $table->integer('tipe_komoditi');
            $table->string('uraian_komoditi',50);
            $table->string('kode_uraian_komoditi',20);
            $table->string('satuan',10);
            $table->float('koefisien');
            $table->float('harga');
            $table->float('nilai');
            $table->timestamps();

            $table->foreign('AHSP_id')
                  ->references('id')->on('AHSP')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Komoditas');
    }
}
