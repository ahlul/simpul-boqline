<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateButirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Butir', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('komponen_id')->unsigned();
            $table->string('kegiatan',50);
            $table->bigInteger('volume')->unsigned();
            $table->string('satuan',10);
            $table->float('harga_satuan');
            // $table->float('jumlah_harga');
            $table->integer('nomor'); //(1,2,3,...)
            $table->integer('subnomor'); //(1a,1b,...)
            $table->string('kode_analisa', 10);
            $table->bigInteger('komoditas_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('komponen_id')
                  ->references('id')->on('Komponen')
                  ->onDelete('cascade');
            $table->foreign('komoditas_id')
                  ->references('id')->on('Komoditas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Butir');
    }
}
