<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomponenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Komponen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('grup_komponen_id')->unsigned();
            $table->string('nama_komponen',50);
            $table->integer('urutan_abjad');
            $table->float('nilai_total_komponen');
            $table->timestamps();

            $table->foreign('grup_komponen_id')
                  ->references('id')->on('GrupKomponen')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Komponen');
    }
}
