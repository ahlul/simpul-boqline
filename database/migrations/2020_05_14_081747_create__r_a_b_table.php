<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRABTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RAB', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kegiatan_utama', 100);
            $table->string('pekerjaan', 50);
            $table->year('tahun');
            $table->string('lokasi', 50);
            $table->float('jumlah_nilai_proyek');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RAB');
    }
}
