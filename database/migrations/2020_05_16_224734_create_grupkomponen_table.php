<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrupkomponenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GrupKomponen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('RAB_id')->unsigned();
            $table->string('nama_grup_komponen',50);
            $table->integer('urutan_romawi');
            $table->timestamps();

            $table->foreign('RAB_id')
                  ->references('id')->on('RAB')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('GrupKomponen');
    }
}
