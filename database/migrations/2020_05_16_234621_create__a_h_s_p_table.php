<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAHSPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AHSP', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dasar_hukum', 25)->unique();
            $table->integer('tahun_anggaran');
            $table->string('file_AHSP', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AHSP');
    }
}
