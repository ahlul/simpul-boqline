<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{ route('frontend.index') }}">
        <img class="navbar-brand-full" src="{{ asset('img/backend/brand/logo.png') }}" width="120" alt="Simpul BoQline Logo">
        <img class="navbar-brand-minimized" src="{{ asset('img/backend/brand/sygnet.svg') }}" width="30" height="30" alt="Simpul BoQline Mini Logo">
    </a>

    <ul class="nav navbar-nav d-md-down-none">
        @auth
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('frontend.entry.index') }}">Entri</a>
        </li>
        @can('view backend')
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('frontend.proses.index') }}">Proses</a>
        </li>
        @endcan
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('frontend.upload.index') }}">Upload</a>
        </li>
        @endauth
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('frontend.info.index') }}">Informasi</a>
        </li>
        @auth
        @can('view backend')
        <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('admin.auth.user.index') }}">Administrasi</a>
        </li>
        @endcan
        @endauth
        {{-- <li class="nav-item px-3">
                <a class="nav-link" href="{{ route('frontend.index') }}"><i class="fas fa-home"></i></a>
        </li> --}}

        {{-- <li class="nav-item px-3">
            <a class="nav-link" href="{{ route('admin.dashboard') }}">@lang('navs.frontend.dashboard')</a>
        </li> --}}

        {{-- @if(config('locale.status') && count(config('locale.languages')) > 1)
            <li class="nav-item px-3 dropdown">
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="d-md-down-none">@lang('menus.language-picker.language') ({{ strtoupper(app()->getLocale()) }})</span>
                </a>

                @include('includes.partials.lang')
            </li>
        @endif --}}
    </ul>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#">
                <i class="fas fa-bell"></i>
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#">
                <i class="fas fa-list"></i>
            </a>
        </li>
        @guest
        <li class="nav-item d-md-down-none">
            <a class="nav-link {{ active_class(Route::is('frontend.auth.login')) }}" role="button" href="{{route('frontend.auth.login')}}">
                <span class="d-md-down-none">Masuk</span>
            </a>
        </li>
        @endguest

        @auth
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="{{ $logged_in_user->picture }}" class="img-avatar" alt="{{ $logged_in_user->email }}">
            <span class="d-md-down-none">{{ $logged_in_user->full_name }}</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            {{-- <div class="dropdown-header text-center"><strong>Account</strong></div> --}}
            <a class="dropdown-item" href="{{ route('frontend.user.account') }}">
                <i class="fas fa-lock"></i> Akun Ku
            </a>
            @can('view backend')
                <a href="{{ route('admin.dashboard') }}" class="dropdown-item"> Menu Administrasi</a>
            @endcan
            <div class="dropdown-divider"></div>    
            <a class="dropdown-item" href="{{ route('frontend.auth.logout') }}">Keluar</a>
          </div>
        </li>
        @endauth
    </ul>

    <div class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
        {{-- <span class="navbar-toggler-icon"></span> --}}
    </div>
    <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
    </button>
</header>
