@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . 'Entri Detail RAB')


@section('content')

{{-- <a href="" class="bg-teal alert d-block text-center text-white py-3" >
<strong>Views: </strong> <code>{{ \Request::route()->getName() }}</code>
</a> --}}

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header bg-pink text-white">
                Form Entri Rancangan Anggaran Belanja (RAB)
                <div class="card-header-actions">
                    <a class="card-header-action" data-toggle="tooltip" title="" data-original-title="Tutup Jendela">
                        <i class="fa fa-window-close"></i>
                    </a>
                </div>
            </div>
            @if ($rab->kegiatan_utama)
            <form class="form-horizontal" action="{{ route('frontend.entry.RAB.detail.update', [$rab->id]) }}"
                method="post" enctype="multipart/form-data"> @method('PATCH')
            @else
            <form class="form-horizontal" action="{{ route('frontend.entry.RAB.detail.store') }}" method="post"
                enctype="multipart/form-data">
            @endif
                <div class="card-body">
                    @csrf
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="activity-i">Kegiatan Utama</label>
                        <div class="col-md-9">
                            <input class="form-control" id="activity-i" type="text" name="kegiatan_utama"
                                value="{{$rab->kegiatan_utama ?? ""}}" required>
                            {{-- <span class="help-block">Masukkan kegiatan utama</span> --}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="workname-i">Pekerjaan</label>
                        <div class="col-md-9">
                            <input class="form-control" id="workname-i" type="text" name="pekerjaan"
                                value="{{$rab->pekerjaan ?? ""}}" required>
                            {{-- <span class="help-block">Masukkan pekerjaan utama</span> --}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="ccyear-i">Tahun</label>
                        <div class="col-md-9">
                            <input class="form-control input-harga" id="ccyear-i"
                                name="tahun" type="number" value="{{$rab->tahun ?? ""}}"
                                required step=1 min=2017 max=2020>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="location-i">Lokasi</label>
                        <div class="col-md-9">
                            <input class="form-control" id="location-i" type="text" name="lokasi"
                                value="{{$rab->lokasi ?? ""}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="jumlah_nilai_proyek-i">Jumlah</label>
                        <div class="col-md-9 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control harga" id="jumlah_nilai_proyek-i"
                                value="{{$rab->jumlah_nilai_proyek ?? ""}}"
                                required step=0.01>
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="ppn10-i">PPN 10%</label>
                        <div class="col-md-9 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control harga" id="ppn10-i" 
                                value="{{ $rab->jumlah_nilai_proyek*0.1 ?? ""}}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="jumlah_total-i">Jumlah Total</label>
                        <div class="col-md-9 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input class="form-control harga" id="jumlah_total-i" 
                                value="{{$rab->jumlah_nilai_proyek+$rab->jumlah_nilai_proyek*0.1 ?? ""}}" disabled>
                        </div>
                    </div>
                </div>
                <input class="form-control harga" name="jumlah_nilai_proyek" value="{{$rab->jumlah_nilai_proyek ?? ""}}" hidden="">
                <div class="card-footer">
                    <button class="btn btn-sm text-white bg-pink" type="submit" style="">
                            <i class="fa fa-dot-circle"></i> Entri</button>
                    @if ($rab->kegiatan_utama)
                    <button class="btn btn-sm float-right" onclick="handlerDeleteDetail()">
                            <i class="fa fa-trash"></i> Hapus</button>
                    @endif
                </div>
            </form>

        </div>
    </div>
</div>

@endsection

@push('after-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
<script>

// $('.harga').toArray().forEach(function (field) {
//     new Cleave(field, {
//         numeral: true,
//         numeralThousandsGroupStyle: 'thousand',
//         prefix: 'Rp.  ',
//     });
// });

$('#jumlah_nilai_proyek-i').number(true, 2);
$('#jumlah_nilai_proyek-i').change( function() {
    var jumlah_nilai_proyek = $('#jumlah_nilai_proyek-i').val()*1;
    var ppn10 = jumlah_nilai_proyek * 0.1
    var jumlah_total = jumlah_nilai_proyek + ppn10;
    console.log(ppn10)
    console.log(jumlah_total)
    $('input[name=jumlah_nilai_proyek]').val(jumlah_nilai_proyek);
    $('#ppn10-i').val($.number(ppn10, 2));
    $('#jumlah_total-i').val($.number(jumlah_total, 2));
})

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function handlerDeleteDetail() {
    $.ajax({
        url: "{{ route('frontend.entry.RAB.detail.delete', [$rab->id ?? 1]) }}",
        type: "POST",
        datatype: 'json',
        data: {
            _token: "{{ csrf_token() }}",
        }
    })
}



</script>
@endpush