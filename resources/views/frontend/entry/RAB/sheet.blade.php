@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . 'Entri RAB Komponen dan Uraian')

@section('container-class', 'container-fluid')

@section('content')

@if ($rab->did_submit)
<a href="" class="bg-indigo alert d-block text-center text-white py-3" >
    <strong>Perhatian! Anda hanya bisa melihat hasil entrian RAB ini</strong>
        {{-- <strong>Views: </strong> <code>{{ \Request::route()->getName() }}</code> --}}
</a>    
@endif

<input type="text" id="daftar-kode-analisa-hi" value="{{ $daftar_kode_analisa ?? '[]' }}" hidden>
<input type="text" id="rab-hi" value="{{ $rab ?? '{}' }}" hidden>
<input type="text" id="daftar-komponen-raw-hi" value="{{ $daftar_komponen ?? '[]' }}" hidden>
<input type="text" id="daftar-butir-hi" value="{{ $daftar_butir ?? '[]' }}"  hidden>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                Outline Entri - Daftar Komponen
                <div class="card-header-actions">
                    <a href="#" class="card-header-action" data-toggle="tooltip" title="Hide">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
            </div>
            <div class="card-body" id="daftar-outline-l">
            </div>
        </div>
    </div>


    <div class="col-12 col-xl-8">
 
            <ul class="nav nav-pills mb-3 mx-4" id="menu-pills-tab" role="tablist">
                {{-- <ul class="nav nav-pills"> --}}
                <li>
                    <a class="btn btn-pill btn-light mr-4" >0. Outline</a>
                </li>
                <li class="nav-item" role="tablist">
                    <a class="btn btn-pill btn-outline-light active" data-toggle="tab" href="#komponen_cards" aria-controls="komponen_cards" aria-selected="true">1. Entri Komponen</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-pill btn-outline-light" data-toggle="tab" href="#uraian_cards" aria-controls="uraian_cards" aria-selected="false">2. Entri Uraian</a>
                </li>
                <li>
                    <a href="#rekap_entrian_cards" class="btn btn-pill btn-outline-light" data-toggle="tab" aria-controls="rekap_entrian_cards" aria-selected="false">3. Rekap Entri</a>
                </li>
            </ul>
        
        <div class="tab-content bg-transparent border-0">

            <!--
              ___    _____                                   __    
             <  /   / ___/__  __ _  ___  ___  ___  ___ ___  / /____
             / /   / /__/ _ \/  ' \/ _ \/ _ \/ _ \/ -_) _ \/ __(_-<
            /_(_)  \___/\___/_/_/_/ .__/\___/_//_/\__/_//_/\__/___/
                                /_/                               
                                    
            -->

            <div class="tab-pane active" id="komponen_cards" role="tabpanel">
                <div class="card" style="display: none;">
                    <div class="card-header">
                        Form Entri Grup Komponen <span class="status"></span>
                        <div class="card-header-actions">
                            <a class="card-header-action" data-toggle="tooltip" title="Tutup Jendela">
                                <i class="fa fa-window-close"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" id="grup-komponen-fe">
                        {{-- <div class="row">
                            <div class="col border border-primary mx-2 my-2 py-1"> --}}
                        <div class="card-group">
                            <div class="card">
                                <div class="card-header bg-transparent border-primary">Grup Komponen</div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="urutan-romawi-i">Nomor</label>
                                        <input class="form-control" id="urutan-romawi-i" type="number">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama-grup-komponen-i">Nama Grup Komponen</label>
                                        <div class="input-group">
                                            <input class="form-control" id="nama-grup-komponen-i" type="text">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fa fa-search"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col border border-info mx-2 my-2 py-1"> --}}
                            <div class="card">
                                <div class="card-header bg-transparent border-info">Komponen</div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="urutan-abjad-i">Nomor</label>
                                        <input class="form-control" id="urutan-abjad-i" type="number">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama-komponen-i">Nama Komponen</label>
                                        <input class="form-control" id="nama-komponen-i" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nilai-total-komponen-i">Nilai Total Komponen</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp</span>
                                            </div>
                                            <input class="form-control harga" id="nilai-total-komponen-i">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-sm btn-primary mt-2" id='simpan-local-perubahan-grup-komponen-data'>
                                <i class="fa fa-dot-circle"></i> Entri</button>
                        <button class="btn btn-sm btn-info mt-2" id='simpan-local-perubahan-komponen-data' >
                                <i class="fa fa-dot-circle"></i> Entri</button>
                        <a class="btn btn-sm btn-danger mt-2 float-right" data-toggle="tooltip" title="Hapus">
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Daftar Grup Pekerjaan</div>
                    <div class="card-body">
                        <div class="alert alert-success" role="alert">This is a success alert—check it out!</div>
                        <div class="row">
                            <div class="col">
                                <table id="daftar-grup-komponen-t" {{-- data-search="true" --}}
                                    class="table table-responsive-sm table-sm">
                                    <thead>
                                        <tr>
                                            <th data-field="urutan_romawi" data-sortable="true">No</th>
                                            <th data-field="nama_grup_komponen" data-sortable="true">Nama Grup Komponen
                                            </th>
                                            <th data-formatter="countKomponenCalc" data-sortable="true">Jumlah Komponen</th>
                                        </tr>
                                    </thead>
                                </table>
                                <button class="btn btn-sm btn-outline-primary tambah mt-2"><i class="fa fa-plus"></i> Tambah</button>
                            </div>

                            <div class="col" style="display: none;">
                                <table id="daftar-komponen-t" {{-- data-search="true" --}}
                                    data-show-footer="true"
                                    class="table table-responsive-sm table-sm">
                                    <thead>
                                        <tr>
                                            <th data-field="urutan_abjad" data-sortable="true">No</th>
                                            <th data-field="nama_komponen" data-sortable="true" data-footer-formatter="teksTotal_FooterFormatter">Nama Komponen</th>
                                            <th data-field="nilai_total_komponen" data-sortable="true"
                                                data-formatter="ribuanFormatter" data-align="right" data-footer-formatter="nilaiTotalKomponen_FooterFormatter">Nilai Total Komponen
                                                (Rp)</th>
                                        </tr>
                                    </thead>
                                </table>
                                <button class="btn btn-sm btn-outline-info tambah mt-2"><i class="fa fa-plus"></i> Tambah</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" id='simpan-perubahan-komponen-raw-data'>
                            <i class="fa fa-save"></i> Simpan Perubahan</button>
                        {{-- <button class="btn btn-sm" id="reset-perubahan-komponen-raw-data">
                            <i class="fa fa-ban"></i> Reset</button> --}}
                    </div>
                </div>
            </div>

            <!--          
               ___      __  __     _ __    
              |_  |    / / / /__  (_) /____
             / __/_   / /_/ / _ \/ / __(_-<
            /____(_)  \____/_//_/_/\__/___/
                                        
            -->
        
            <div class="tab-pane" id="uraian_cards" role="tabpanel">

                <div class="alert alert-secondary alert-dismissible fade show" role="alert">
                    <strong>Bila anda tidak dapat melihat isian tabel dibawah ini.</strong> silahkan pilih link pada bagian <em>outline</em> terlebih dahulu.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                {{-- <div class="card"> --}}
                <div class="card" style="display: none;">
                    <div class="card-header bg-purple text-white" >
                        Form Entri Uraian Pekerjaan <span class="status"></span>
                        <div class="card-header-actions">
                            <a class="card-header-action" data-toggle="tooltip" title="Tutup Jendela">
                                <i class="fa fa-window-close"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body" id="uraian-pekerjaan-fe">
                        <div class="form-group form-row">
                            <label class="col" for="nomor-i">Nomor</label>
                            <div class="col-md-9"><input class="form-control form-control-sm" id="nomor-i" type="text"
                                    ></div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col" for="kegiatan-i">Kegiatan</label>
                            <div class="col-md-9">
                                <textarea class="form-control form-control-sm" id="kegiatan-i" type="text" rows="3"
                                    placeholder="Kegiatan"></textarea>
                            </div>

                        </div>
                        <div class="form-group form row">
                            <label class="col-md-3" for="kode-analisa-i">Kode Analisa</label>
                            <div class="col">
                                <div class="input-group">
                                    <div id="bloodhound">
                                        <input class="form-control form-control-sm typeahead" id="kode-analisa-i" type="text" placeholder="Kode Analisa">
                                    </div>
                                    <div class="input-group-append">
                                        <button class="btn btn-sm btn-outline-secondary" id="kode-analisa-apply-btn" type="button"><i class="fa fa-search"></i> Cari</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <span class="align-middle text-muted" id="deskripsi-kode-analisa-label">
                                    <em>kode analisa belum terpilih</em>
                                </span>
                            </div>
                            {{-- <div class="col-md-6">Keterangan : <p>1 M Pagar Sementara dari Seng Gelombang tinggi 2 m</p></div> --}}
                        </div>
                        <div class="form-group form-row">
                            <label class="col-md-3" for="volume-i">Volume</label>
                            {{-- <div class="col">
                                <input class="form-control form-control-sm harga" id="volume-i" type="text"
                                    placeholder="Volume">
                            </div>
                            <label class="col-md-3" for="satuan-i">Satuan</label>
                            <div class="col">
                                <input class="form-control form-control-sm" id="satuan-i" type="text"
                                    placeholder="Satuan">
                            </div> --}}
                            <div class="col input-group input-group-sm">
                                <input class="form-control form-control-sm harga" id="volume-i" type="text" placeholder="Volume">
                                <div class="input-group-append">
                                    <span class="input-group-text">Satuan</span>
                                </div>
                                <input class="form-control form-control-sm" id="satuan-i" type="text" placeholder="Satuan">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-md-3" for="harga-satuan-i">Harga Satuan</label>
                            <div class="col input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input class="form-control harga" id="harga-satuan-i"
                                    type="text" placeholder="Harga per satuan">
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <label class="col-md-3" for="jumlah-harga-i">Jumlah Harga (Rp)</label>
                            <div class="col input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <button class="btn btn-sm btn-outline-secondary" id="hitung-jumlah-harga-btn" type="button"><i class="fa fa-calculator"></i> Hitung</button>
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input class="form-control harga" id="jumlah-harga-i"
                                    type="text" placeholder="Jumlah Harga" disabled>
                            </div>
                        </div>

                        <button class="btn btn-sm btn-primary bg-purple mt-2" id='simpan-local-perubahan-butir-data'>
                                <i class="fa fa-dot-circle"></i> Entri</button>
                        <a class="btn btn-sm btn-danger mt-2 float-right" data-toggle="tooltip" title="Hapus">
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">Daftar Uraian Pekerjaan <span class="float-right sumber"></span></div>
                    <div class="card-body" style="display: none;">
                        <div class="alert alert-success" role="alert">This is a success alert—check it out!</div>
                        <div class="percentages">
                            <div class="clearfix">
                                <div class="float-right">
                                    <small class="text-muted">Persentase</small> <strong>70%</strong>
                                </div>
                            </div>
                            <div class="progress mb-1">
                                <div class="progress-bar bg-purple" role="progressbar" style="width: 50%"
                                    aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <table id="daftar-uraian-pekerjaan-t" {{-- data-search="true" --}}
                            data-show-footer="true"
                            class="table table-responsive-sm table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th data-field="nomor" data-sortable="true">No</th>
                                    <th data-field="kegiatan" data-sortable="true" data-footer-formatter="teksTotal_FooterFormatter">Kegiatan</th>
                                    <th data-field="kode_analisa" data-sortable="true">Kode Analisa</th>
                                    <th data-field="volume" data-sortable="true" data-formatter="ribuanFormatter"
                                        data-align="right">Volume</th>
                                    <th data-field="satuan" data-sortable="true">Satuan</th>
                                    <th data-field="harga_satuan" data-sortable="true" data-formatter="ribuanFormatter"
                                        data-align="right" >Harga Satuan (Rp)</th>
                                    <th data-sortable="true" data-formatter="jumlahCalc" data-align="right" data-footer-formatter="jumlah_FooterFormatter">Σ Harga (Rp)</th>
                                </tr>
                            </thead>
                        </table>
                        <button class="btn btn-sm btn-outline-purple tambah mt-2"><i class="fa fa-plus"></i> Tambah</button>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary bg-purple" id='simpan-perubahan-butir-raw-data'>
                            <i class="fa fa-save"></i> Simpan Perubahan</button>
                        {{-- <button class="btn btn-sm" type="reset" id='reset-perubahan-butir-data'>
                            <i class="fa fa-ban"></i> Reset</button> --}}
                    </div>
                </div>
            </div>

            <!-- 
                 ____     ___      __               ____     __      _         
                |_  /    / _ \___ / /_____ ____    / __/__  / /_____(_)__ ____ 
              _/_ <_   / , _/ -_)  '_/ _ `/ _ \  / _// _ \/ __/ __/ / _ `/ _ \
            /____(_) /_/|_|\__/_/\_\\_,_/ .__/ /___/_//_/\__/_/ /_/\_,_/_//_/
                                        /_/                                   
            -->         

            <div class="tab-pane" id="rekap_entrian_cards" role="tabpanel">
                <div class="alert alert-secondary alert-dismissile fade show" role="alert">
                    Lakukan pengiriman entrian jika hanya anda sudah yakin bahwa entrian yang dimasukkan <strong>sudah sesuai dengan data RAB yang diajukan</strong>.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="card">
                    <div class="card-header bg-primary text-white">
                        Pengiriman Entrian Ke Admin
                        <div class="card-header-actions">
                            <a class="card-header-action" data-toggle="tooltip" title="Tutup Jendela">
                                <i class="fa fa-window-close"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <table id='detail-rab-overview-t' class="table table-sm table-clear">
                                    <tbody><tr>
                                    <td class="left">
                                        <strong>Id</strong>
                                    </td>
                                    <td class="right" data-name="id">1</td>
                                </tr><tr>
                                    <td class="left">
                                        <strong>Kegiatan Utama</strong>
                                    </td>
                                    <td class="right" data-name="kegiatan_utama">Pembangunan Gedung Kota</td>
                                </tr><tr>
                                    <td class="left">
                                        <strong>Pekerjaan</strong>
                                    </td>
                                    <td class="right" data-name="pekerjaan">pembangunan umum</td>
                                </tr><tr>
                                    <td class="left">
                                        <strong>Tahun</strong>
                                    </td>
                                    <td class="right" data-name="tahun">2021</td>
                                </tr><tr>
                                    <td class="left">
                                        <strong>Lokasi</strong>
                                    </td>
                                    <td class="right" data-name="lokasi">Kecamatan Metro Utara</td>
                                </tr><tr>
                                    <td class="left">
                                        <strong>Jumlah Nilai Proyek</strong>
                                    </td>
                                    <td class="right" data-name="jumlah_nilai_proyek">687629090.91</td>
                                </tr></tbody>
                                </table>
                            </div>
                            <div class="col col-sm-12">
                                <a class="btn btn-primary text-white" href="{{ route('frontend.entry.RAB.send', [$rab->id]) }}">
                                    <i class="fa fa-usd"></i> Kirim Hasil Entri</a>
                                <a class="btn btn-outline-primary"  href="{{ route('frontend.entry.RAB.unsend', [$rab->id]) }}">
                                    <i class="fa fa-usd"></i> Batal Kirim Hasil Entri</a>
                            </div>
                        </div>
                    </div>
                </div>

                @if (!$rab->did_submit)
                <div class="card">
                    <div class="card-header">
                        Daftar Uraian Yang Perlu dicek kembali
                        <div class="card-header-actions">
                            <button type="button" class="btn btn-primary card-header-action text-white" onclick="handlerGetValids(_token)"><span class="cui-contrast"></span> Proses Uraian</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="alert alert-success" role="alert">Silahkan pilih tombol <em>Proses Uraian</em> untuk melihat apakah ada uraian yang perlu divalidasi kembali.</div>
                        <table id="daftar-validasi-t" 
                            data-search="true" 
                            data-flat="true" 
                            data-toggle="table"
                            data-show-pagination-switch="true"
                            data-show-toggle="true"
                            data-show-fullscreen="true"
                            data-show-columns="true"
                            class="table table-responsive-sm table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th data-field="urutan_romawi" data-sortable="true">GK</th>
                                    <th data-field="urutan_abjad" data-sortable="true">K</th>
                                    <th data-field="nomor" data-sortable="true">No</th>
                                    <th data-field="kegiatan" data-sortable="true">Kegiatan</th>
                                    <th data-field="kode_analisa" data-sortable="true">Kode Analisa</th>
                                    <th data-field="komponen_pekerjaan" data-sortable="true">Komponen Pekerjaan</th>
                                    <th data-field="volume" data-sortable="true" data-formatter="ribuanFormatter"
                                        data-align="right">Volume</th>
                                    <th data-field="satuan" data-sortable="true">Satuan</th>
                                    <th data-field="komponen_harga_satuan_pekerjaan" data-formatter="ribuanFormatter"
                                        data-sortable="true">Komponen Harga Satuan Pekerjaan</th>
                                    <th data-field="harga_satuan" data-sortable="true" data-formatter="ribuanFormatter"
                                        data-align="right">Harga Satuan (Rp)</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                @endif

            </div>


        </div>
    </div>
</div>


@endsection

{{-- ======================================================================================= --}}

@push('before-styles')
<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/magnific-popup.min.css">
<link rel="stylesheet" href="https://unpkg.com/is-loading@2.0.4/css/index.css">
@endpush

@push('after-styles')
<!-- Bootstrap -->
<style> 
    .btn-outline-purple {
        color: var(--purple);
        border-color: var(--purple);
    }
    .btn-outline-purple:hover {
        color: #fff;
        background-color: var(--purple);
        border-color: var(--purple);
    }
    .dialog {
        z-index: 1088;
        position: relative;
        background: #FFF;
        padding: 20px;
        width:auto;
        max-width: 500px;
        margin: 20px auto;
    }
    .no-click {
        pointer-events:none
    }
    </style>

<!-- Typeahead Custom CSS -->
<style> 
    /* https://github.com/bassjobsen/typeahead.js-bootstrap4-css/blob/master/typeaheadjs.css */
    span.twitter-typeahead .tt-menu {
        cursor: pointer;
    }

    .dropdown-menu,
    span.twitter-typeahead .tt-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 160px;
        padding: 5px 0;
        margin: 2px 0 0;
        font-size: 0.765625rem;
        color: #373a3c;
        text-align: left;
        list-style: none;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.25rem;
    }

    span.twitter-typeahead .tt-suggestion {
        display: block;
        width: 100%;
        padding: 3px 20px;
        clear: both;
        font-weight: normal;
        line-height: 1.5;
        color: #373a3c;
        text-align: inherit;
        white-space: nowrap;
        background: none;
        border: 0;
    }

    span.twitter-typeahead .tt-suggestion:focus,
    .dropdown-item:hover,
    span.twitter-typeahead .tt-suggestion:hover {
        color: #2b2d2f;
        text-decoration: none;
        background-color: #f5f5f5;
    }

    span.twitter-typeahead .active.tt-suggestion,
    span.twitter-typeahead .tt-suggestion.tt-cursor,
    span.twitter-typeahead .active.tt-suggestion:focus,
    span.twitter-typeahead .tt-suggestion.tt-cursor:focus,
    span.twitter-typeahead .active.tt-suggestion:hover,
    span.twitter-typeahead .tt-suggestion.tt-cursor:hover {
        color: #fff;
        text-decoration: none;
        background-color: #0275d8;
        outline: 0;
    }

    span.twitter-typeahead .disabled.tt-suggestion,
    span.twitter-typeahead .disabled.tt-suggestion:focus,
    span.twitter-typeahead .disabled.tt-suggestion:hover {
        color: #818a91;
    }

    span.twitter-typeahead .disabled.tt-suggestion:focus,
    span.twitter-typeahead .disabled.tt-suggestion:hover {
        text-decoration: none;
        cursor: not-allowed;
        background-color: transparent;
        background-image: none;
        filter: "progid:DXImageTransform.Microsoft.gradient(enabled = false)";
    }

    span.twitter-typeahead {
        width: 100%;
    }

    .input-group span.twitter-typeahead {
        display: block !important;
    }

    .input-group span.twitter-typeahead .tt-menu {
        top: 2.375rem !important;
    }

    </style>
@endpush

@push('before-scripts')
@endpush


@push('after-scripts')
{{-- <script src="https://cdn.jsdelivr.net/npm/table-to-json@1.0.0/lib/jquery.tabletojson.min.js" integrity="sha256-H8xrCe0tZFi/C2CgxkmiGksqVaxhW0PFcUKZJZo1yNU=" crossorigin="anonymous"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.3.1/bloodhound.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.3.1/typeahead.jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js"></script>
<script src="https://unpkg.com/is-loading@2.0.4/dist/isLoading.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@gorillastack/jmespath@1.3.0/jmespath.js"></script>

{!! script('js/sheet.custom.js') !!}

<script>
    _token = "{{ csrf_token() }}"
    _url = {
        'frontend.entry.RAB.sheet.components.commit': 
            "{{ route('frontend.entry.RAB.sheet.components.commit', $rab->id) }}",
        'frontend.entry.RAB.sheet.units.commit': (num) => {
            return "{{ route('frontend.entry.RAB.sheet.units.commit', ['id' => $rab->id, 'num' => 1]) }}".slice(0, -1) + komponen_terpilih_id(num)
        },
        'frontend.proses.valids.show': (num) => {
            return "{{ route('frontend.proses.valids.show', ['id' => 1] ) }}".slice(0, -1) + num    
        },
    }
</script>

@endpush