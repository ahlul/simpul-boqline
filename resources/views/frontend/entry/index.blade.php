@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . 'Manajemen Entri RAB')

@section('content')

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">Rancangan Anggaran Belanja (RAB)
                {{-- <div class="card-header-actions">
                <a class="card-header-action" href="{{ route('frontend.entry.RAB.detail.create') }}">
                    <small class="text-muted">Tambah</small>
                </a>
                </div> --}}
            </div>
            <div class="card-body">
                <table class="table table-responsive-sm table-sm table-hover mb-0">
                    <thead class="bg-pink text-white">
                        <tr>
                            <th>ID</th>
                            <th class="w-25">Kegiatan Utama</th>
                            <th class="w-25">Pekerjaan</th>
                            <th>Lokasi</th>
                            <th>Tahun</th>
                            <th>Telah Dikirim</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($RAB as $thisRAB)
                        <tr>
                            <td>{{ $thisRAB->id }}</td>
                            <td>{{ $thisRAB->kegiatan_utama }}</td>
                            <td>{{ $thisRAB->pekerjaan }}</td>
                            <td>{{ $thisRAB->lokasi }}</td>
                            <td>{{ $thisRAB->tahun }}</td>
                            <td>
                                @if ($thisRAB->did_submit)
                                    <span class="badge badge-success">Terkirim</span>
                                @else
                                    <span class="badge badge-warning">Belum Terkirim</span>
                                @endif
                            </td>
                            <td class="btn-td">
                                <div class="btn-group btn-group-sm" role="group" aria-label="User Actions">
                                    <a href="{{ route('frontend.entry.RAB.detail.edit', [$thisRAB->id]) }}" target="_blank" data-toggle="tooltip"  class="btn btn-sm btn-outline-pink" data-original-title="Lihat">
                                        <i class="fas fa-eye"></i>
                                    </a>

                                    <a href="{{ route('frontend.entry.RAB.sheet', $thisRAB->id) }}" target="_blank"
                                        data-toggle="tooltip" class="btn btn-sm btn-dark" data-original-title="Entri">
                                        <i class="fas fa-edit"></i>
                                    </a>

                                    <div class="btn-group" role="group">
                                        <button id="userActions" type="button"
                                            class="btn btn-sm btn-outline-pink dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            Lainnya </button>
                                        <div class="dropdown-menu" aria-labelledby="userActions" style="">
                                            <a href="{{ route('frontend.entry.RAB.detail.edit', [$thisRAB->id]) }}" class="dropdown-item">Ubah Rincian RAB</a>
                                            <div class="dropdown-divider"></div>    
                                            <a href="{{ route('frontend.entry.RAB.send', [$thisRAB->id]) }}" class="dropdown-item">Kirim Entri</a>
                                            <a href="{{ route('frontend.entry.RAB.unsend', [$thisRAB->id]) }}" class="dropdown-item">Batal Kirim Entri</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <a href="{{ route('frontend.entry.RAB.detail.create') }}" class="btn btn-sm btn-outline-pink tambah mt-2">
                    <i class="fa fa-plus"></i> Tambah</a>
            </div>
        </div>
    </div>
</div>


{{-- <h1>this is Select</h1> --}}
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header bg-dark">Analisa Harga Standar Pekerjaan (AHSP)</div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="boqlist" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                            aria-controls="home" aria-selected="true">2020</a>
                    </li>
                </ul>
                <div class="tab-content" id="boqlistContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <dl class="row">
                            <dt class="col-sm-3">ID AHSP</dt>
                            <dd class="col-sm-9">1</dd>

                            <dt class="col-sm-3">Dasar Hukum</dt>
                            <dd class="col-sm-9">PERMEN PU NOMOR 28 TAHUN 2016</dd>

                            <dt class="col-sm-3">Tahun Anggaran</dt>
                            <dd class="col-sm-9">2019</dd>

                            <dt class="col-sm-3 text-truncate">Truncated term is truncated</dt>
                            <dd class="col-sm-9">Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum
                                nibh, ut fermentum massa justo sit amet risus.</dd>

                            <dt class="col-sm-3">Aksi</dt>
                            <dd class="col-sm-9">
                                <dl class="row">
                                    <dt class="col-sm-4">Download AHSP ini..</dt>
                                    <dd class="col-sm-8">Link</dd>

                                    <dt class="col-sm-4">Ubah/Edit AHSP</dt>
                                    <dd class="col-sm-8"><em>(hanya dapat dilakukan oleh Admin)</em></dd>
                                </dl>
                        </dd> 
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection




@push('after-styles')
<!-- Bootstrap -->
<style> 
    .btn-outline-pink {
        color: var(--pink);
        border-color: var(--pink);
    }
    .btn-outline-pink:hover {
        color: #fff;
        background-color: var(--pink);
        border-color: var(--pink);
    }
</style>
@endpush