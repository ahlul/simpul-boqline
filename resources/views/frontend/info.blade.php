@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')

<a href="" class="bg-teal alert d-block text-center text-white py-3" >
<strong>Views: </strong> <code>{{ \Request::route()->getName() }}</code>
</a>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Informasi Sistem Pengumpulan Data Bill of Quantity Online
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-4">
                        <div class="list-group" id="list-tab" role="tablist">
                            <a class="list-group-item list-group-item-action active show" id="list-home-list"
                                data-toggle="tab" href="#list-home" role="tab" aria-controls="list-home"
                                aria-selected="true">Pengertian BoQ</a>
                            <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="tab"
                                href="#list-profile" role="tab" aria-controls="list-profile"
                                aria-selected="false">Publikasi IKK</a>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="list-home" role="tabpanel"
                                aria-labelledby="list-home-list">

<p><strong><u>Pengertian <em>Bill of Quantity</em> (BoQ)</u></strong></p>
<p><strong><em>Bill of Quantity</em></strong><strong> (BoQ)</strong> adalah dokumen yang
    berisi daftar uraian pekerjaan, kuantitas pekerjaan, serta biaya yang dibutuhkan
    dalam suatu proyek pekerjaan konstruksi. Komponen data <em>Bill of Quantity
    </em>berisikan tiga hal pokok yaitu daftar harga satuan upah, bahan, dan sewa
    peralatan; analisa harga satuan pekerjaan (AHSP); dan rancangan angran biaya (RAB)
    atau daftar kuantitas harga.</p>
<p>Analisa biaya dalam proyek konstruksi sering kita sebut dengan <strong>Analisa Harga
        Satuan Pekerjaan (AHSP).</strong> AHSP sendiri banyak macamnya, diantaranya AHSP
    yang dikeluarkan oleh Ditjend Bina Marga Kementrian Pekerjaan Umum atau analisa SNI
    berdasarkan Peraturan Menteri Pekerjaan Umum Republik Indonesia yang terakhir yaitu
    Permen PUPR Nomor 28 tahun 2016 tentang Pedoman Analisis Harga Satuan Pekerjaan
    Bidang Pekerjaan Umum. Analisa SNI adalah pedoman perhitungan analisa harga satuan
    pekerjaan yang selalu mengikuti perkembangan standar nasional atau spesifikasi
    teknis pekerjaan konstruksi. Disebut pedoman berarti menjadi petunjuk dalam
    perhitungan, akan tetapi pedoman tersebut tetap harus disesuaikan dengan kondisi
    dimana pekerjaan konstruksi direncanakan atau akan di bangun.</p>
<p>Pada AHSP setiap pekerjaan per satuan pekerjaan, unsur pembentuknya terdiri dari kode
    analisa; rincian kebutuhan per komoditi antara lain tenaga kerja, bahan/bahan,
    peralatan, beserta satuan standar masing-masing komoditi; koefisien setiap komoditi;
    harga satuan setiap komoditi; jumlah yang merupakan perkalian antara nilai koefisien
    dengan harga satuan setiap komoditi; <em>overhead</em> atau profit yang biasanya
    dihitung sebesar 10-15 % dari total jumlah kebutuhan seluruh komoditi. Total nilai
    jumlah kebutuhan seluruh komoditi ditambahkan dengan <em>overhead</em> atau profit
    disebut dengan jumlah harga per satuan pekerjaan.</p>
<p><strong>Rancangan Anggaran Biaya (RAB) </strong>&nbsp;atau daftar kuantitas harga
    adalah perhitungan banyaknya biaya yang dibutuhkan untuk setiap jenis uraian
    pekerjaan yang&nbsp; terdiri dari upah tenaga kerja, bahan, sewa peralatan, serta
    biaya-biaya lain yang berhubungan dengan pelaksanaan bangunan atau proyek. RAB
    memiliki beberapa komponen di dalamnya, yaitu:</p>
<ol>
    <li>Uraian pekerjaan. Merupakan uraian seluruh jenis perkerjaan yang dilakukan dalam
        pelaksanaan suatu proyek dari awal sampai dengan selesai. Jika pekerjaan
        konstruksi biasanya terdapat sub jenis pekerjaan misalnya pekerjaan persiapan,
        galian, urugan, pekerjaan pondasi beton, dan lainnya.</li>
</ol>
<ol start="2">
    <li>Kode analisa. Merupakan kode unik untuk setiap jenis uraian pekerjaan
        berdasarkan Permen PUPR Nomor 28 tahun 2016 tentang Pedoman Analisis Harga
        Satuan Pekerjaan Bidang Pekerjaan Umum.</li>
    <li>Volume pekerjaan. Merupkan jumlah unit setiap uraian pekerjaan yang dibutuhkan
        untuk menyelesaikan suatu proyek. Jumlah volume yang digunakan memiliki satuan
        dengan karakteristk masing-masing uraian pekerjaan. Jika di dalam pengadaan
        barang biasanya digunakan satuan unit. Sedangkan untuk pekerjaan konstruksi
        kebanyakan dihitung dalam satuan meter persegi (m2), meter kubik (m3), atau
        lumpsum (Ls).</li>
    <li>Harga satuan. Merupakan harga satuan standar dari suatu uraian pekerjaan, yang
        dihitung berdasarkan Analisis Harga Satuan Pekerjaan (AHSP).</li>
    <li>Jumlah harga. Merupakan perkalian antara harga satuan dengan volume pekerjaan.
    </li>
</ol>
<p><strong>Harga Satuan Upah, Bahan, dan Sewa Peralatan </strong>merupakan suatu daftar
    yang merinci harga standar setiap jenis upah tenaga kerja, bahan-bahan konstruksi,
    serta biaya sewa peralatan yang berlaku di suatu wilayah pada waktu tertentu. Harga
    satuan upah, bahan, dan sewa peralatan suatu wilayah pada suatu waktu biasanya
    ditentukan berdasarkan keputusan kepala daearah masing-masing daerah.</p>
<p><strong><u>Manfaat Data <em>Bill of Quantity</em> (BoQ)</u></strong></p>
<p>Data BoQ merupakan salah satu data penting yang sangat diperlukan dalam penghitungan
    data Indeks Kemahalan Konstruksi (IKK). Data BoQ digunakan sebagai salah satu
    komponen untuk membentuk nilai penimbang di dalam proses penghitungan angka IKK.
    Sementara itu, IKK sendiri merupakan indeks spasial yang menggambarkan tingkat
    kemahalan konstruksi suatu kabupaten/kota di Indonesia dibandingkan dengan kota
    acuan yaitu Kota Semarang. Data IKK diperoleh dari hasil Survei Harga Kemahalan
    Konstruksi khusus bahan bangunan/konstruksi, sewa alat berat, dan upah jasa
    konstruksi yang dilaksanakan di seluruh kabupaten/kota di Indonesia. Peran angka IKK
    sangat strategis dalam perumusan salah satu kebijakan keuangan pemerintah, karena
    digunakan di dalam penghitungan dan penentuan Dana Alokasi Umum (DAU) setiap tahun.
</p>




</div>
<div class="tab-pane fade active show" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">


    <div class="media">
        <img class="mr-3"
            src="{{ asset('img/frontend/info/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2019_1.jpg') }}"
            width="155px" alt="">
        <div class="media-body">
            <h5 class="mt-0">Publikasi IKK 2019</h5>
            <a href="{{ Storage::url('documents/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2019.pdf1590216980.pdf') }}">Unduh disini</a>
        </div>
    </div>

    <div class="media">
        <img class="mr-3"
            src="{{ asset('img/frontend/info/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2018_1.jpg') }}"
            width="155px" alt="">
        <div class="media-body">
            <h5 class="mt-0">Publikasi IKK 2018</h5>
            <a href="{{ Storage::url('documents/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2018.pdf1590216980.pdf') }}">Unduh disini</a>
        </div>
    </div>

    <div class="media">
        <img class="mr-3"
            src="{{ asset('img/frontend/info/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2017_1.jpg') }}"
            width="155px" alt="">
        <div class="media-body">
            <h5 class="mt-0">Publikasi IKK 2017</h5>
            <a href="{{ Storage::url('documents/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2017.pdf1590216979.pdf') }}">Unduh disini</a>
        </div>
    </div>
    <div class="media">
        <img class="mr-3"
            src="{{ asset('img/frontend/info/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2016_1.jpg') }}"
            width="155px" alt="">
        <div class="media-body">
            <h5 class="mt-0">Publikasi IKK 2016</h5>
            <a href="{{ Storage::url('documents/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2016.pdf1590216979.pdf') }}">Unduh disini</a>
        </div>
    </div>

    <div class="media">
        <img class="mr-3"
            src="{{ asset('img/frontend/info/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2015_1.jpg') }}"
            width="155px" alt="">
        <div class="media-body">
            <h5 class="mt-0">Publikasi IKK 2015</h5>
            <a href="{{ Storage::url('documents/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2015.pdf1590216979.pdf') }}">Unduh disini</a>
        </div>
    </div>


    <div class="media">
        <img class="mr-3"
            src="{{ asset('img/frontend/info/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2014_1.jpg') }}"
            width="155px" alt="">
        <div class="media-body">
            <h5 class="mt-0">Publikasi IKK 2014</h5>
            <a href="{{ Storage::url('documents/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2014.pdf1590216979.pdf') }}">Unduh disini</a>
        </div>
    </div>

    <div class="media">
        <img class="mr-3"
            src="{{ asset('img/frontend/info/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2013_1.jpg') }}"
            width="155px" alt="">
        <div class="media-body">
            <h5 class="mt-0">Publikasi IKK 2013</h5>
            <a href="{{ Storage::url('documents/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2013.pdf1590216979.pdf') }}">Unduh
                disini</a>
        </div>
    </div>
    <div class="media">
        <img class="mr-3"
            src="{{ asset('img/frontend/info/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2012_1.jpg') }}"
            width="155px" alt="">
        <div class="media-body">
            <h5 class="mt-0">Publikasi IKK 2012</h5>
            <a href="{{ Storage::url('documents/Indeks Kemahalan Konstruksi Provinsi dan Kabupaten_Kota 2012.pdf1590216979.pdf') }}">Unduh disini</a>
        </div>
    </div>
</div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('after-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.5.10/cleave.min.js"></script>
<script>


</script>
@endpush