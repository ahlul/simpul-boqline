    @extends('frontend.layouts.app')

    @section('title', app_name() . ' | ' . __('navs.general.home'))

    @section('content')
<div class="row mb-4">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <i class="fas fa-home"></i> @lang('navs.general.home')
            </div>
            <div class="card-body">

<div class="media">
<img class="mr-3" src="{{ asset('img/frontend/illustration-home.png') }}" alt="Generic placeholder image">
<div class="media-body">
        <h1><u>Selamat Datang di {{ app_name() }}</u></h1>
    <h5 class="mt-0">Sistem Pengumpulan Data <em>Bill of Quantity Online</em></h5>
    <p><strong> Sistem Pengumpulan Data <em>Bill of Quantity Online</em></strong> adalah aplikasi untuk pengumpulan data Bill of Quantity (BoQ) yang dapat dilakukan secara online. Sistem ini dapat mempermudah, mempercepat, dan meningkatkan akurasi pengumpulan data BoQ, karena responden hanya perlu melakukan entri data Rancangan Anggaran Biaya (RAB) suatu proyek pekerjaan. Sistem akan otomatis menggabungkan data RAB dengan data Analisis Harga Satuan Pekerjaan (AHSP) yang sudah tersedia di database untuk kemudian menjadi data BoQ sesuai dengan standar pengumpulan data yang dibutuhkan oleh Badan Pusat Statitsik.
    </p>
    <p>AHSP yang ada sudah mengakomodir analisa sesuai dengan Peraturan Menteri Pekerjan Umum Republik Indonesia dan harga satuan upah, bahan, dan sewa peralatan yang berlaku pada tahun anggaran proyek</p>
</div>
</div>
                

                
            </div>
        </div>
    </div>
</div>




{{-- <div class="row mb-4">
        <div class="col">
            <example-component></example-component>
        </div><!--col-->
    </div><!--row--> --}}
{{-- 
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <i class="fab fa-font-awesome-flag"></i> Font Awesome @lang('strings.frontend.test')
            </div>
            <div class="card-body">
                <i class="fas fa-home"></i>
                <i class="fab fa-facebook"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-pinterest"></i>
            </div>
            <!--card-body-->
        </div>
        <!--card-->
    </div>
    <!--col-->
</div>
<!--row--> --}}

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Daftar <em>Rancangan Anggaran Belanja (RAB)</em>
                <div class="card-header-actions">
                    <a class="card-header-action" href="https://coreui.io/docs/components/bootstrap-list-group/"
                        target="_blank">
                        <small class="text-muted">docs</small>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">Morbi leo risus</li>
                    <li class="list-group-item">Porta ac consectetur ac</li>
                    <li class="list-group-item">Vestibulum at eros</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Daftar <em>Analisa Harga Standar Pekerjaan (AHSP)</em>
                    <div class="card-header-actions">
                        <a class="card-header-action" href="https://coreui.io/docs/components/bootstrap-list-group/"
                            target="_blank">
                            <small class="text-muted">docs</small>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item">Cras justo odio</li>
                        <li class="list-group-item">Dapibus ac facilisis in</li>
                        <li class="list-group-item">Morbi leo risus</li>
                        <li class="list-group-item">Porta ac consectetur ac</li>
                        <li class="list-group-item">Vestibulum at eros</li>
                    </ul>
                </div>
            </div>
        </div>
</div>


@endsection
