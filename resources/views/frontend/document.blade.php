@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')

<a href="" class="bg-teal alert d-block text-center text-white py-3" >
<strong>Views: </strong> <code>{{ \Request::route()->getName() }}</code>
</a>

<div id="ui-view">
    <div>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12 mb-4">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#upload" role="tab"
                                aria-controls="upload">Upload</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#download" role="tab"
                                aria-controls="download">Download</a>
                        </li>
                    </ul>


<div class="tab-content">
    <div class="tab-pane active" id="upload" role="tabpanel">
        <form class="form-horizontal" action="{{ route('frontend.upload.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
        <div class="form-group form-row">
            <label class="col" for="kegiatan-i">Keterangan Dokumen</label>
            <div class="col-md-9">
                <textarea class="form-control form-control-sm" id="keterangan-dokumen-i" name="keterangan_dokumen" type="text" rows="3"
                    placeholder="Judul Dokumen"></textarea>
            </div>
        </div>

        <div class="form-group form-row">
            <label class="col-md-3" for=dokumen-i">File</label>
            <div class="col"><input class="form-control form-control-sm harga" name="dokumen[]" id=dokumen-i" type="file" multiple
                    placeholder="Harga per satuan"></div>
        </div>

        <div class="form-group form-row">
            <label class="col-md-3" for="kirim-i"></label>
            <div class="col"><button id="kirim-i" type="submit" class="btn btn-block btn-danger" type="button">Upload</button></div>
        </div>
        </form>


    </div>
    <div class="tab-pane" id="download" role="tabpanel">

<table class="table table-responsive-sm">
    <thead>
        <tr>
            <th>Keterangan File</th>
            <th>Nama File</th>
            <th>Diupload pada</th>
            <th>Unduh</span>
            </th>
        </tr>
        @foreach ($docs as $doc)
        <tr>
            <td>{{ $doc->keterangan_dokumen }}</td>
            <td>{{ $doc->lokasi }}</td>
            <td>{{ $doc->created_at }}</td>
            <td><span class="badge badge-danger"><a href="{{ Storage::url($doc->lokasi) }}">Unduh</a></span></td>
        </tr>
        @endforeach
        {{-- <tr>
            <td rowspan="2">Chetan Mohamed</td>
            <td>2012/02/01</td>
            <td>Staff</td>
            <td>
                <span class="badge badge-danger">Unduh</span>
            </td>
        </tr>
        <tr>
            <td>2012/02/01</td>
            <td>Admin</td>
            <td>
                <span class="badge badge-secondary">Inactive</span>
            </td>
        </tr>
        <tr>
            <td>Derick Maximinus</td>
            <td>2012/03/01</td>
            <td>Member</td>
            <td>
                <span class="badge badge-warning">Pending</span>
            </td>
        </tr>
        <tr>
            <td>Friderik Dávid</td>
            <td>2012/01/21</td>
            <td>Staff</td>
            <td>
                <span class="badge badge-success">Active</span>
            </td>
        </tr> --}}
    </tbody>
</table>

    </div>
</div>


                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('after-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.5.10/cleave.min.js"></script>
<script>


</script>
@endpush