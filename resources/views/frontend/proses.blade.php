@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . 'Proses BoQ')

@section('container-class', 'container-fluid')

@section('content')

<a href="" class="bg-teal alert d-block text-center text-white py-3" id="kegiatan-utama-judul">
    RAB Terpilih : <strong>(Belum Terpilih)</strong>
    {{-- <strong>Views: </strong> <code>{{ \Request::route()->getName() }}</code> --}}
</a>

<input type="text" id="daftar-rab-hi" value="{{ $daftar_rab ?? '[]' }}" hidden>
<input type="text" id="ahsp-hi" value="{{ $ahsp ?? '{}' }}" hidden>


<div class="container">
    <div class="row aksi-proses-boq justify-content-center">

        <div class="col-3">
            <div class="card validasi-c">
                <div class="card-body p-3 d-flex align-items-center">
                    <i class="fa fa-cogs bg-primary p-3 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-primary">
                            Validasi
                        </div>
                        {{-- <div class="text-muted text-uppercase font-weight-bold small"></div> --}}
                    </div>
                </div>
                <div class="card-footer px-3 py-2 ">
                    <button class="btn-block text-muted d-flex justify-content-between align-items-center dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="font-weight-bold">Lakukan</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item py-1" href="#" data-toggle="modal" data-target="#modal-validasi-rab">Pilih RAB..</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item py-1 simpan" href="#">Simpan validasi</a>
                        <a class="dropdown-item py-1 finalisasi" href="#">Finaliasi</a>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade pilih-rab-m" id="modal-validasi-rab" tabindex="-1" role="dialog" aria-labelledby="modal-validasi-rab-title" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                    <div class="modal-header py-2">
                        <h5 class="modal-title" id="modal-validasi-rab-title">Pilih RAB</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer py-2">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-primary" id='generate-data-daftar-validasi'>Pilih</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="card entri-sistem-c">
                <div class="card-body p-3 d-flex align-items-center">
                    <i class="fa fa-laptop bg-info p-3 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-info">Entri Sistem</div>
                        <div class="text-muted text-uppercase font-weight-bold small"></div>
                    </div>
                </div>
                <div class="card-footer px-3 py-2">
                    <button class="btn-block text-muted d-flex justify-content-between align-items-center dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="font-weight-bold">Lakukan</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item py-1" href="#" data-toggle="modal" data-target="#modal-entri-sistem">Pilih RAB..</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item py-1 simpan" href="#">Simpan entri sistem</a>
                        <a class="dropdown-item py-1 finalisasi" href="#">Finaliasi</a>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade pilih-rab-m" id="modal-entri-sistem" tabindex="-1" role="dialog" aria-labelledby="modal-entri-sistem-title" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                    <div class="modal-header py-2">
                        <h5 class="modal-title" id="modal-entri-sistem-title">Pilih RAB</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer py-2">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-primary" id='generate-data-daftar-kode-sistem'>Pilih</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="card">
                <div class="card-body p-3 d-flex align-items-center">
                    <i class="fa fa-moon bg-warning p-3 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-warning">Generate BoQ</div>
                    </div>
                </div>
                <div class="card-footer px-3 py-2">
                        <button class="btn-block text-muted d-flex justify-content-between align-items-center dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="font-weight-bold">Lakukan</span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item py-1" href="#" data-toggle="modal" data-target="#modal-generate-boq">Pilih RAB..</a>
                        </div>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade pilih-rab-m" id="modal-generate-boq" tabindex="-1" role="dialog" aria-labelledby="modal-generate-boq-title" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                    <div class="modal-header py-2">
                        <h5 class="modal-title" id="modal-generate-boq-title">Pilih RAB</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer py-2">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-primary" id='generate-data-boq'>Pilih</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- <div class="col-3">
            <div class="card">
                <div class="card-body p-3 d-flex align-items-center">
                    <i class="fa fa-bell bg-danger p-3 font-2xl mr-3"></i>
                    <div>
                        <div class="text-value-sm text-danger">Lihat Hasil BoQ</div>
                    </div>
                </div>
                <div class="card-footer px-3 py-2">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="#">
                        <span class="font-weight-bold">Lakukan</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div> --}}

    </div>
</div>

<div class="row">
    
    <div class="col-12 window" style="display:none;">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Validasi 
                <div class="card-header-actions">
                    <a class="card-header-action" data-toggle="tooltip" title="Tutup Jendela">
                        <i class="fa fa-window-close"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                {{-- <div class="alert alert-success" role="alert">This is a success alert—check it out!</div> --}}
                <table id="daftar-validasi-t" {{-- data-search="true" --}} data-flat="true" data-toggle="table"
                    data-click-to-select="true" class="table table-responsive-sm table-bordered table-sm">
                    <thead>
                        <tr>
                            <th data-field="invalid" data-checkbox="true"></th>
                            <th data-field="urutan_romawi" data-sortable="true">GK</th>
                            <th data-field="urutan_abjad" data-sortable="true">K</th>
                            <th data-field="nomor" data-sortable="true">No</th>
                            <th data-field="kegiatan" data-sortable="true">Kegiatan</th>
                            <th data-field="kode_analisa" data-sortable="true">Kode Analisa</th>
                            <th data-field="komponen_pekerjaan" data-sortable="true">Komponen Pekerjaan</th>
                            <th data-field="volume" data-sortable="true" data-formatter="ribuanFormatter"
                                data-align="right">Volume</th>
                            <th data-field="satuan" data-sortable="true">Satuan</th>
                            <th data-field="komponen_harga_satuan_pekerjaan" data-formatter="ribuanFormatter"
                                data-sortable="true">Komponen Harga Satuan Pekerjaan</th>
                            <th data-field="harga_satuan" data-sortable="true" data-formatter="ribuanFormatter"
                                data-align="right">Harga Satuan (Rp)</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="col-12 window" style="display:none;">
        {{-- <div class="card">
            <div class="card-header bg-info text-white">
                Entri Kode Sistem
                <div class="card-header-actions">
                    <a class="card-header-action" data-toggle="tooltip" title="Tutup Jendela">
                        <i class="fa fa-window-close"></i>
                    </a>
                </div>
            </div>
            <div class="card-body"> --}}

        <div class="row">
            <div class="col">
                <div class="card" id="kode-sistem-fe">
                    <div class="card-header">Daftar Komponen Kode Sistem</div>
                    <div class="card-body">
                        <table id="daftar-kode-sistem-t" {{-- data-search="true" --}} data-group-by="true"
                            data-group-by-field="shape" data-click-to-select="true" class="table table-responsive-sm table-sm">
                            <thead>
                                <tr>
                                    <th data-field="selected" data-checkbox="true"></th>
                                    <th data-field="urutan_romawi" data-sortable="true">No</th>
                                    <th data-field="urutan_abjad" data-sortable="true">No</th>
                                    <th data-field="nama_grup_komponen" data-sortable="true">GK</th>
                                    <th data-field="nama_komponen" data-sortable="true">Nama Komponen</th>
                                    <th data-field="kode_sistem" data-sortable="true" data-align="right">Kode Sistem</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
            <div class="col">
                <div class="card" id="kode-sistem-fe">
                    <div class="card-header">
                        Rekapitulasi Kode Sistem
                        <div class="card-header-actions">
                            <a class="card-header-action" data-toggle="tooltip" title="Tutup Jendela">
                                <i class="fa fa-window-close"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="daftar-outline-kode-sistem-t" {{-- data-search="true" --}}
                            data-show-footer=true
                            class="table table-responsive-sm table-sm">
                            <thead>
                                <tr>
                                    <th data-field="kode_sistem" data-sortable="true">Kode</th>
                                    <th data-formatter="namaKodeSistemFormatter">Nama Sistem</th>
                                    <th data-field="nilai" data-formatter="ribuanFormatter" data-sortable="true" data-footer-formatter="nilaiKodeSistem_FooterFormatter">Nilai</th>
                                    <th data-formatter="persenKodeSistemFormatter" data-sortable="true" data-footer-formatter="persenKodeSistem_FooterFormatter">Persentase</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <div class="card" id="kode-sistem-fe">
                    <div class="card-header bg-info text-white">Form Entri Kode Sistem</div>
                    <div class="card-body">
                        <label for="kode-sistem-i">Pilih Kode Entri yang sesuai untuk setiap baris terpilih</label>
                        <div class="input-group">
                            <select class="form-control" id="kode-sistem-i">
                                <option value="1">Sitework/Persiapan</options>
                                <option value="2">Substructure</options>
                                <option value="3">Superstructure</options>
                                <option value="4">Exterior Shell</options>
                                <option value="5">Interior Partitions</options>
                                <option value="6">Interior and Exterior Finishes</options>
                                <option value="7">Mechanical & Plumbing</options>
                                <option value="8">Electrical</options>
                            </select>
                            <span class="input-group-append">
                                <button class="btn btn-primary" id="simpan-local-kode-sistem" type="button">Entri</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 window container" style="display:none;">
            <div class="container">

        <div class="card" id="detail-boq-c">
            <div class="card-header bg-warning text-white">BOQ Terpilih</div>
            <div class="card-body">
                <div class="row">
                    <div class="col col-lg-8 col-sm-12">
                        <table id="detail-rab-overview-t" class="table table-sm table-clear">
                            <tbody>
                                <tr>
                                    <td class="left">
                                        <strong>Id</strong>
                                    </td>
                                    <td class="right" data-name="id">2</td>
                                </tr>
                                <tr>
                                    <td class="left">
                                        <strong>Kegiatan Utama</strong>
                                    </td>
                                    <td class="right" data-name="kegiatan_utama">Pembangunan gorong-gorong</td>
                                </tr>
                                <tr>
                                    <td class="left">
                                        <strong>Pekerjaan</strong>
                                    </td>
                                    <td class="right" data-name="pekerjaan">dua tingkat</td>
                                </tr>
                                <tr>
                                    <td class="left">
                                        <strong>Tahun</strong>
                                    </td>
                                    <td class="right" data-name="tahun">2018</td>
                                </tr>
                                <tr>
                                    <td class="left">
                                        <strong>Lokasi</strong>
                                    </td>
                                    <td class="right" data-name="lokasi">sukadana</td>
                                </tr>
                                <tr>
                                    <td class="left">
                                        <strong>Jumlah Nilai Proyek</strong>
                                    </td>
                                    <td class="right" data-name="jumlah_nilai_proyek">300000</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col col-lg-4 col-sm-12 ml-auto">
                        <a class="btn btn-warning" id="export-hasil-boq">
                            <i class="fa fa-usd"></i> Export Hasil BOQ terpilih</a>
                    </div>
                </div>
            </div>
        </div>
            </div>
    </div>

    <div class="col-12 window" style="display:none;">
        <div class="card">
            <div class="card-header bg-danger text-white">Title Jendela</div>
            <div class="card-body"></div>
        </div>
    </div>
</div>

@endsection

@push('after-styles')
<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/extensions/group-by-v2/bootstrap-table-group-by.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/magnific-popup.min.css">
<link rel="stylesheet" href="https://unpkg.com/toaster-js/default.css"/>
<style>
.dialog {
        z-index: 1088;
        position: relative;
        background: #FFF;
        padding: 20px;
        width:auto;
        max-width: 500px;
        margin: 20px auto;
    }
.toast {
    background-color: rgba(255, 255, 255, 0) !important;
    border: 1px solid rgba(0, 0, 0, 0) !important;
    box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, 0) !important;
    /* Original  */
    position: fixed;
    right: 0;
    bottom: 0;
    z-index: 10000;
    max-width: 100%;
    opacity: 0;
    transform: translate(75%, 0);
    pointer-events: none;
    -webkit-transition: all 0.3s ease, transform 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
    -moz-transition: all 0.3s ease, transform 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
    -o-transition: all 0.3s ease, transform 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
    transition: all 0.3s ease, transform 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
    }
    .toast.displayed {
        opacity: 1;
        transform: translate(0, 0);
    }
    .toast>.body {
        position: relative;
        font-size: initial;
        margin: 0 1em 1em 1em;
        padding: .5em;
        word-wrap: break-word;
        border-radius: 3px;
        background: rgba(255, 255, 255, 0.9);
        pointer-events: all;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.5);
    }
    .toast>.body.info {
        background: rgba(255, 245, 195, 0.9);
    }
    .toast>.body.warning {
        background: rgba(255, 183, 99, 0.9);
    }
    .toast>.body.warning>.icon {
        color: white;
    }
    .toast>.body.error {
        color: white;
        text-shadow: 0 0 1px black;
        background: rgba(255, 86, 86, 0.9);
    }
    .toast>.body.done {
        background: rgba(147, 255, 157, 0.9);
    }
</style>
@endpush

@push('after-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/extensions/group-by-v2/bootstrap-table-group-by.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.15/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.3.1/bloodhound.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.3.1/typeahead.jquery.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/toaster-js/umd.js"></script>
<script src="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@gorillastack/jmespath@1.3.0/jmespath.js"></script>


{!! script('js/xlsx-populate-no-encryption.min.js') !!}
{!! script('js/xlsx-datafill.js') !!}
<script src="https://cdn.jsdelivr.net/npm/json2csv"></script>

<script>

_token = "{{ csrf_token() }}"
_url = {
    'frontend.proses.valids.show': (num) => {
        return "{{ route('frontend.proses.valids.show', ['id' => 1] ) }}".slice(0, -1) + num    },
    'frontend.proses.valids.update': (num) => {
        return "{{ route('frontend.proses.valids.update', ['id' => 1] ) }}".slice(0, -1) + num    },
    'frontend.proses.syscode.show': (num) => {
        return "{{ route('frontend.proses.syscode.show', ['id' => 1] ) }}".slice(0, -1) + num    },
    'frontend.proses.syscode.update': (num) => {
        return "{{ route('frontend.proses.syscode.update', ['id' => 1] ) }}".slice(0, -1) + num    },
    'frontend.proses.boq.show': (num) => {
        return "{{ route('frontend.proses.boq.show', ['id' => 1] ) }}".slice(0, -1) + num    },
    'frontend.proses.boq.store': (num) => {
        return "{{ route('frontend.proses.boq.store', ['id' => 1] ) }}".slice(0, -1) + num    }
}

// RAB terpilih harus nantinya pisah sendiri yaa

rab_terpilih_id = -1
daftar_RAB_data = JSON.parse($('#daftar-rab-hi').val())
AHSP_data = JSON.parse($('#ahsp-hi').val())

rab_terpilih_index = () => daftar_RAB_data.findIndex((el) => (el.id == rab_terpilih_id))
// daftar_validasi_raw_data = JSON.parse($('#daftar-validasi-hi').val())
daftar_validasi_raw_data = []
daftar_validasi_data = () => { return daftar_validasi_raw_data }
daftar_validasi_invalid_arr = () => {
    return $('#daftar-validasi-t').bootstrapTable('getAllSelections').map((el) => el.id)
}

daftar_kode_sistem_raw_data = []
daftar_kode_sistem_data = () => { return daftar_kode_sistem_raw_data }
daftar_outline_kode_sistem_data = () => {
    return _(daftar_kode_sistem_raw_data)
                // harus diubah yaa
                .groupBy('kode_sistem')
                .map((objs, key) => {
                    return {
                        'kode_sistem': objs[0].kode_sistem,
                        'grup_komponen_id': objs[0].grup_komponen_id,
                        // 'grup_komponen_id':_(objs).uniqBy('grup_komponen_id').head().property('grup_komponen_id'),
                        'nilai': objs.reduce((acc, row) => acc + parseFloat(row.nilai_total_komponen), 0)
                    }
                })
                .value()
}
kode_sistem_terpilih_id_arr = () => {
    return $('#daftar-kode-sistem-t').bootstrapTable('getAllSelections').map((el) => el.id)
}
kode_sistem_terpilih_index_arr = () => {
    return daftar_kode_sistem_raw_data.map((obj, key) => {
        if (_.includes(kode_sistem_terpilih_id_arr(), obj.id)) return key
    }).filter(el => typeof el !== 'undefined')
}
total_dari_nilai_total_komponen = () => {
    return daftar_kode_sistem_raw_data.reduce((acc, row) => acc + parseFloat(row.nilai_total_komponen), 0)
}

boq_raw_data = []
boq_data = () => { 
    return {
        'detail': rab_terpilih_index() > -1 ? daftar_RAB_data[rab_terpilih_index()] : [],
        'rows': boq_raw_data,
        'codes': AHSP_data,
        'outlines': boq_rekap_kodesistem()
    } 
}

boq_rekap_kodesistem = () => {
    let ks_per_units = jmespath.search(
        boq_raw_data, 
        // "[*].components[*].{ kode_sistem : kode_sistem, harga_satuan : sum(units[*].harga_satuan[]), volume : sum(units[*].volume[]) }[]"
        "[*].components[*].{ kode_sistem : kode_sistem, harga_satuan : sum(units[*].harga_satuan.to_number(@)), volume : sum(units[*].volume.to_number(@)) }[]"
    )
    // _(ks_per_units).groupBy('kode_sistem').map( (obj, key) => {return { 'total': _.sumBy(obj, 'uu') }}).value()
    return _(ks_per_units).map((el) => {
                    el.nilai = el.harga_satuan * el.volume; 
                    return el;
                }).groupBy('kode_sistem').map((obj, key) => {
                    return {
                        'ks': key, 
                        'nilai_total' : _.sumBy(obj, 'nilai')
                    }
                }).value()
}

function jumlahCalc(value, row) {
    let newval = row.volume * row.harga_satuan
    return '<em>' + $.number(newval, 2, ',', ' ') + '</em>'
}

function ribuanFormatter(value, row) {
    return $.number(value, 2, ',', ' ')
}

function countKomponenCalc(value, row) {
    return row.components.filter((el) => !el.hasOwnProperty('deleted')).length
}

function namaKodeSistemFormatter(value, row) {
    let def_kodesistem = ["- Kosong -","Sitework/Persiapan","Substructure","Superstructure","Exterior Shell","Interior Partitions","Interior and Exterior Finishes","Mechanical & Plumbing","Electrical"]
    return `<em> ${def_kodesistem[row.kode_sistem]} </em>`
}

function nilaiKodeSistem_FooterFormatter(data) {
    return $.number(
                data.reduce((acc, row) => acc + parseFloat(row.nilai), 0),
                2, ',', ' ')
}

function persenKodeSistemFormatter(value, row) {
    let newval = (row.nilai / total_dari_nilai_total_komponen()) * 100
    // console.log(newval)
    return '<em>' + $.number(newval, 2, ',', ' ') + '</em>'
}

function persenKodeSistem_FooterFormatter(data) {
    return 100.0
}


// 

$("#generate-data-boq").on('click', function(event) {
    rab_terpilih_id = $('input[name=rab-terpilih-id-i]:checked').val()
    $('#kegiatan-utama-judul strong').html(daftar_RAB_data[rab_terpilih_index()].kegiatan_utama)
    $('.window:has(#daftar-validasi-t)').fadeOut()
    $('.window:has(#daftar-kode-sistem-t)').fadeOut()
    $('.window:has(#detail-boq-c)').fadeIn()
    $('#modal-generate-boq').modal('hide')
    let newToast = new Toast("mengunduh data BOQ");
    render('detail-generate-boq');
    handlerGetBOQ(_token, rab_terpilih_id)
})

$("#export-hasil-boq").on('click', function(event) {
    let newToast = new Toast("memproses data BOQ");
    generateBOQReportXLSX(boq_data())
})
// generateBOQReportXLSX(boq_data())




$('#daftar-kode-sistem-t').bootstrapTable({
    data: daftar_kode_sistem_data(),
    groupBy: true,
    groupByField: 'grup_komponen_id',
    groupByFormatter: (value, idx, data) => {
        // console.log(data)
        return `<strong>${convertToRoman(data[0].urutan_romawi)}. ${data[0].nama_grup_komponen}</strong>`
    }
})
$('#daftar-outline-kode-sistem-t').bootstrapTable({
    data: daftar_outline_kode_sistem_data()
})
$("#generate-data-daftar-kode-sistem").on('click', function(event) {
    rab_terpilih_id = $('input[name=rab-terpilih-id-i]:checked').val()
    $('#kegiatan-utama-judul strong').html(daftar_RAB_data[rab_terpilih_index()].kegiatan_utama)
    handlerGetEntrySystem(_token, rab_terpilih_id)
    $('.window:has(#daftar-validasi-t)').fadeOut()
    $('.window:has(#detail-boq-c)').fadeOut()
    $('.window:has(#daftar-kode-sistem-t)').fadeIn()
    $('#modal-entri-sistem').modal('hide')
    let newToast = new Toast("menampilkan tabel entri sistem terpilih");
})
$(".window:has(#daftar-outline-kode-sistem-t) a:has(.fa-window-close)").on('click', (event) => {
    $('.window:has(#daftar-outline-kode-sistem-t)').fadeOut()
})
$("#simpan-local-kode-sistem").on('click', (event) => {
    for (const idx of kode_sistem_terpilih_index_arr()) {
        daftar_kode_sistem_raw_data[idx].kode_sistem = $("#kode-sistem-i").val()
    }
    render('table-daftar-kode-sistem')
})

$(".entri-sistem-c .simpan").click((e) => commitEntrySystemChanges(_token, rab_terpilih_id))

// _________________________________________________
//   _    _     _                
//  ' )  /     //     /          
//   (  /__.  // o __/ __.  _   o
//    \/(_/|_</_<_(_/_(_/|_/_)_<_
//                             
// __ Validasi

$('#daftar-validasi-t').bootstrapTable({
    data: daftar_validasi_data()
})
$("#generate-data-daftar-validasi").on('click', function(event) {
    rab_terpilih_id = $('input[name=rab-terpilih-id-i]:checked').val()
    $('#kegiatan-utama-judul strong').html(daftar_RAB_data[rab_terpilih_index()].kegiatan_utama)
    handlerGetValids(_token, rab_terpilih_id)
    $('.window:has(#daftar-kode-sistem-t)').fadeOut()
    $('.window:has(#detail-boq-c)').fadeOut()
    $('.window:has(#daftar-validasi-t)').fadeIn()
    let rab = daftar_RAB_data.find((e)=>e.id == rab_terpilih_id)
    $(".aksi-proses-boq .validasi-c .card-body .text-muted").html(`${rab.kegiatan_utama} (${rab.tahun})`)
    $('#modal-validasi-rab').modal('hide')
    let newToast = new Toast("menampilkan tabel validasi terpilih");
})
render('daftar-rab-l') // to all daftar-rab-l
// $('#daftar-validasi-t').bootstrapTable('getAllSelections')

$(".window:has(#daftar-validasi-t) a:has(.fa-window-close)").on('click', (event) => {
    // render('refresh-all-tables')
    $('.window:has(#daftar-validasi-t)').fadeOut()
})

$(".validasi-c .simpan").click((e) => commitValidsChanges(_token, rab_terpilih_id))

// const json2csvParser = new json2csv.Parser()
// mycsv = json2csvParser.parse(daftar_validasi_raw_data);

// _________________________________________________
//
//    __                       _   ,   _  () _   ,
//   /  `                _/_  ' \ /  _//  /\' \ / 
//  /--  _.,  _   __ __  /       X   /   /  )  X  
// (___,/ /\_/_)_(_)/ (_<__     / \_/___/__/__/ \_
//          /                                     
//         '                                      
// __ Export XlSX of BOQ

excel_data = []
excel_template_url = "{{ Storage::url('templates/boq-template.xlsx') }}"
$.getJSON("{{ Storage::url('templates/boq-data.json') }}", (data) => {excel_data = data})

function generateBOQReportXLSX(ed = excel_data) {
    // Helper functions
    parseCell = function (cell) {
        getFirstDigitPosition = (string) => string.search(/\d/);
        intToExcelCol = (number) => {
                var colName = '',
                    dividend = Math.floor(Math.abs(number)),
                    rest;
                while (dividend > 0) {
                    rest = (dividend - 1) % 26;
                    colName = String.fromCharCode(65 + rest) + colName;
                    dividend = parseInt((dividend - rest)/26);
                }
                return colName;
            };
        excelColToInt = (colName) => {
                var digits = colName.toUpperCase().split(''),
                    number = 0;
                for (var i = 0; i < digits.length; i++) {
                    number += (digits[i].charCodeAt(0) - 64)*Math.pow(26, digits.length - i - 1);
                }
                return number;    
            }

        switch (typeof cell) {
            case 'string':
                return {
                    column: excelColToInt(cell.slice(0, getFirstDigitPosition(cell))),
                    row: parseInt(cell.slice(getFirstDigitPosition(cell)), 10),
                };
            case 'object':
                return intToExcelCol(cell.column) + cell.row; 
            default:
                throw new Error('Invalid argument type');
        }
    }
    createRange = function (sc, shift_right = 0, shift_down = 0) {
        return _([sc,
            parseCell({
                column: parseCell(sc).column + shift_right,
                row: parseCell(sc).row + shift_down
            })
        ]).join(":")
    }

    var Promise = XlsxPopulate.Promise;
    XlsxPopulateAccess = XlsxDataFill.XlsxPopulateAccess;
    console.log(ed)

    getWorkbook().then(function (workbook) {
        console.log('in workbook')
        console.log(ed)

        window.workbook = workbook

        // insert detail
        window.workbook
            .sheet(0)
                .cell('D3')
                    .value(boq_data().detail.kegiatan_utama)
                .relativeCell(1, 0)
                    .value(boq_data().detail.pekerjaan)
                .relativeCell(1, 0)
                    .value(boq_data().detail.lokasi)
                .relativeCell(1, 0)
                    .value(boq_data().detail.tahun)

        // insert ooutlines
        for (out_idx=1; out_idx<9; out_idx++) {
            let cur_outline = boq_data().outlines.find((el) => el.ks == out_idx)
            window.workbook
                .sheet(0)
                    .cell(`U${11 + out_idx}`)
                        .value(cur_outline ? cur_outline.nilai_total : 0)
        }
        // the undefined category
        if (boq_data().outlines.findIndex((el) => el.ks == 'null') !== -1)
            window.workbook
                .sheet(0)
                    .cell('U22')
                        .value(
                            boq_data().outlines.find((el) => el.ks == 'null').nilai_total
                        )

        // insert boq rows
        // start point!
        sheet_cursor = 'A12'
        json_cursor = 'rows'
        // for gc_idx {
        for (gc_idx=0; gc_idx<_(ed).get(json_cursor).length; gc_idx++) {
            // grup_komponen
            json_cursor += `.${gc_idx}`
            __urutan_romawi = _(ed).get(json_cursor+'.urutan_romawi')
            sheet_cursor = window.workbook 
                .sheet(0)
                    .cell(sheet_cursor)     // 'A12'
                        .value(_(ed).get(json_cursor+'.urutan_romawi'))
                        .style("bold", true)
                    .relativeCell(0, 1)     // 'B12'
                        .value(_(ed).get(json_cursor+'.nama_grup_komponen'))
                        .style("bold", true)
                    .relativeCell(1, 0)
                        .address()
            console.log([json_cursor, sheet_cursor])
            // console.log(_(ed).get(json_cursor))

            // komponen
            json_cursor += ".components"
            if (_(ed).get(json_cursor).length > 0) {
                // for c_idx {
                for (c_idx=0; c_idx<_(ed).get(json_cursor).length; c_idx++) {
                    json_cursor += `.${c_idx}`
                    __urutan_abjad = _(ed).get(json_cursor+'.urutan_abjad')
                    __kode_sistem = _(ed).get(json_cursor+'.kode_sistem')
                    sheet_cursor = window.workbook 
                        .sheet(0)
                            .cell(sheet_cursor)     // 'B13';
                            .relativeCell(0, -1)    // 'A13'
                                .value(`${__urutan_romawi}.${__urutan_abjad}`)
                            .relativeCell(0, 1)     // 'B13'
                                .value(_(ed).get(json_cursor+'.nama_komponen'))
                            .relativeCell(1, 0)
                                .address()
                    console.log([json_cursor, sheet_cursor])
                    // console.log(_(ed).get(json_cursor))
                    

                    // units
                    json_cursor += ".units"
                    if (_(ed).get(json_cursor).length > 0) {
                        // for u_idx {
                        for (u_idx=0; u_idx<_(ed).get(json_cursor).length; u_idx++) {
                            json_cursor += `.${u_idx}`
                            __kode_analisa = _(ed).get(json_cursor+'.kode_analisa')
                            console.log([json_cursor, sheet_cursor])
                            sheet_cursor = window.workbook 
                                .sheet(0)
                                    .cell(sheet_cursor)     // 'B13'
                                    .relativeCell(0, -1)    // 'A14'
                                        .value(`${__urutan_romawi}.${__urutan_abjad}.${_(ed).get(json_cursor+'.nomor')}`)
                                    .relativeCell(0, 1)     
                                        .value(_(ed).get(json_cursor+'.kegiatan'))
                                    .relativeCell(0, 3)     
                                        .value(__kode_analisa)
                                    .relativeCell(0, 1)     
                                        .value(_(ed).get(json_cursor+'.volume')*_(ed).get(json_cursor+'.harga_satuan'))
                                    .relativeCell(0, 1)     
                                        .value(_(ed).get(json_cursor+'.volume'))
                                    .relativeCell(0, 1)     
                                        .value(_(ed).get(json_cursor+'.harga_satuan'))
                                    .relativeCell(0, 1)     
                                        .value(__kode_sistem)
                                    .address()              // H14
                            console.log([json_cursor, sheet_cursor])
                            // console.log(_(ed).get(json_cursor))
                            
                            // komoditas
                            // __kode_analisa = 'A.2.2.1.2'
                            if (_.has(excel_data.codes, __kode_analisa)) {
                                // to column 'J'
                                karr = _.get(excel_data.codes, __kode_analisa)
                                komoditas_cursor = ""
                                sheet_cursor = window.workbook.sheet(0) 
                                        .cell(sheet_cursor).row().cell(10)
                                        .address()          // J14
                                // console.log([komoditas_cursor, sheet_cursor])

                                sheet_cursor = window.workbook 
                                    .sheet(0)
                                        .cell(sheet_cursor)    // J14
                                        .relativeCell(0, 1)    // K14
                                            .value(_(karr).get(komoditas_cursor+'0.komponen_pekerjaan'))
                                            .style("bold", true)
                                        .relativeCell(1, 0)     // K15
                                        .address()
                                // console.log([komoditas_cursor, sheet_cursor])

                                        
                                gbarr = _.groupBy(karr, 'tipe_komoditi')
                                // for ai_idx {
                                for (ai_idx=0; ai_idx<Object.keys(gbarr).length; ai_idx++) {
                                    // groupby_cursor += '1'
                                    groupby_cursor = Object.keys(gbarr)[ai_idx]      // '1'
                                    let interpret_tk = [ '',
                                        'Tenaga', 'Bahan', 'Peralatan',
                                        'Jumlah', 'Overhead & Profit', 'Harga Satuan Pekerjaan']
                                    sheet_cursor = window.workbook 
                                        .sheet(0)
                                            .cell(sheet_cursor)     // K15
                                                .value(interpret_tk[parseInt(groupby_cursor)])
                                            .address()
                                    // console.log(groupby_cursor)
                                    // console.log(_(gbarr).get(groupby_cursor))
                                    // console.log([groupby_cursor, sheet_cursor])

                                    // for aj_idx {                 // where.tipe_komoditi=1
                                    for (aj_idx=0; aj_idx < _.get(gbarr, groupby_cursor).length; aj_idx++) {
                                        window.workbook.sheet(0)
                                                .cell(sheet_cursor)     // K15
                                                .relativeCell(0, -1)    // J15
                                                    .value(_(gbarr).get(groupby_cursor+`.${aj_idx}.kode_uraian_komoditi`))
                                                .relativeCell(0, 2)     // L15
                                                    .value(_(gbarr).get(groupby_cursor+`.${aj_idx}.uraian_komoditi`))
                                                .relativeCell(0, 1)     
                                                    .value(_(gbarr).get(groupby_cursor+`.${aj_idx}.koefisien`))
                                                .relativeCell(0, 1)     
                                                    .value(_(gbarr).get(groupby_cursor+`.${aj_idx}.satuan`))
                                                .relativeCell(0, 1)     
                                                    .value(_(gbarr).get(groupby_cursor+`.${aj_idx}.harga`))
                                                .relativeCell(0, 1)     // P15
                                                    .value(_(gbarr).get(groupby_cursor+`.${aj_idx}.nilai`))
                                        // console.log(groupby_cursor)
                                        // console.log(_(gbarr).get(groupby_cursor))
                                        sheet_cursor = window.workbook
                                            .sheet(0)
                                                .cell(sheet_cursor)
                                                .relativeCell(1, 0)
                                                .address()
                                        // console.log([groupby_cursor, sheet_cursor])
                                    }
                                    // groupby_cursor = _(groupby_cursor).toPath().initial().join('.')
                                    // ai_idx
                                }
                                
                                // -> column 'A's
                                sheet_cursor = window.workbook
                                        .sheet(0)
                                            .cell(sheet_cursor)
                                            .row().cell(2)
                                                .value('current')
                                            .address()

                            } else {
                                if (__kode_analisa == 'Ls') {
                                    console.log([json_cursor, sheet_cursor])
                                    let rrr = window.workbook.sheet(0).cell(sheet_cursor).row().cell(10) // J14
                                    let vvv = rrr.row().cell(2).rangeTo(rrr.row().cell(9)).value()
                                    // rrr.value() // K14
                                    rrr.value([[undefined, vvv[0][0], undefined, vvv[0][5], vvv[0][3], vvv[0][6], vvv[0][4]]])
                                    sheet_cursor = window.workbook
                                                .sheet(0)
                                                    .cell(sheet_cursor)     // J14
                                                    .relativeCell(1, 0)     // H14
                                                        .row().cell(2)      // 
                                                    .address()
                                } else {
                                    // -> dowm -> to column 'A'
                                    sheet_cursor = window.workbook
                                        .sheet(0)
                                            .cell(sheet_cursor)     // J14
                                            .relativeCell(1, 0)
                                                .row().cell(2)      // B15
                                            .address()
                                }
                            }
                            // go down and back to column 'A's
                            console.log("go down and back to column 'B's")
                            console.log([json_cursor, sheet_cursor])
                            json_cursor = _(json_cursor).toPath().initial().join('.')
                            // u_idx++
                        }
                    } //endif
                    console.log("changing to another component :sweat_smile:")
                    console.log([json_cursor, sheet_cursor])
                    json_cursor = _(json_cursor).toPath().initial().initial().join('.')
                    // c_idx++
                }
            }
            // down -> down
            sheet_cursor = window.workbook 
                    .sheet(0)
                        .cell(sheet_cursor)
                        .relativeCell(2, -1)
                            .address()
            console.log("changing to another component_group :sweat_smile:")
            console.log([json_cursor, sheet_cursor])
            json_cursor = _(json_cursor).toPath().initial().initial().join('.')
            // gc_idx++
        }
                
        return window.workbook.outputAsync()
    }).then((blob) => {
        var url = window.URL.createObjectURL(blob);
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.href = url;
        a.download = "out.xlsx";
        a.click();
        window.URL.revokeObjectURL(url);
        document.body.removeChild(a);
    })
}
// generateBOQReportXLSX(excel_data)

// ________________________________________________________________
//
//  _ __                        ______             _              
//  ' )  )            /            /               //     _/_      
//   /--' _  ____  __/ _  __    --/_  ______  _   // __.  /  _  _  
//  /  \_</_/ / <_(_/_</_/ (_  (_/</_/ / / <_/_)_</_(_/|_<__</_/_)_
//                                         /                      
//                                        '                       
// __ Render Templates

function render(template_name, data = {}) {
    let tpl = ""
    switch (template_name) {
        case 'table-daftar-validasi':
            $('#daftar-validasi-t').bootstrapTable('load', daftar_validasi_data())
            break
        case 'table-daftar-kode-sistem':
            $('#daftar-kode-sistem-t').bootstrapTable('load', daftar_kode_sistem_data())
            $('#daftar-outline-kode-sistem-t').bootstrapTable('load', daftar_outline_kode_sistem_data())
            break
        case 'daftar-rab-l':
            let i = 0
            for (const rab of daftar_RAB_data) {
                tpl += `<div class="form-check">
                            <input class="form-check-input" type="radio" name="rab-terpilih-id-i" id="radio-rab-${i}"
                                value="${rab.id}">
                            <label class="form-check-label" for="radio-rab-${i}">
                                ${rab.kegiatan_utama} (${rab.tahun})
                            </label>
                        </div>`
                i++
            }
            $('.pilih-rab-m .modal-body').html(tpl)
            break
        case 'detail-generate-boq':
            // tpl = Object.entries(boq_data().detail).reduce((acc, el) => {
            //     return acc + `<tr>
            //                 <td class="left">
            //                     <strong>${_.upperFirst(_.lowerCase(el[0]))}</strong>
            //                 </td>
            //                 <td class="right">${el[1]}</td>
            //             </tr>`
            // }, '')
            // $('#detail-boq-c tbody').html(tpl)
            let l = $('#detail-rab-overview-t .right')
            for (let i = 0; i < l.length; i++) {
                const $element = $(l[i]);
                $element.text(boq_data().detail[$element.attr('data-name')])
            }
            break
    }
}

// ________________________________________________________________
//    __  ___   __  _   ,             _ __                         
//   /  )(   > /  )' \ /             ' )  )                  _/_   
//  /--/  __/_/--/    X  --- _  _     /--' _  _,  . . _  _   /  _  
// /  (_ / / /  (_   / \_   </_/_)_  /  \_</_(_)_(_/_</_/_)_<__/_)_
//      <_/                                   />                   
//                                          |/                    
// __ AJAX-es Requests

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// async function handlerGetValids(self, e) {
    // e.preventDefault();
async function handlerGetValids(_token, rab_terpilih_id) {
    const result = await $.ajax({
        url: _url['frontend.proses.valids.show'](rab_terpilih_id),
        type: "GET",
        datatype: 'json',
        data: {
            _token: _token,
        }
    })
    daftar_validasi_raw_data = JSON.parse(result)
    $('.card-body:has(#daftar-validasi-t) .alert').html('true')
    render('table-daftar-validasi')
    // return false;
}

async function commitValidsChanges(_token, rab_terpilih_id) {
    const result = await $.ajax({
        url: _url['frontend.proses.valids.update'](rab_terpilih_id),
        type: "POST",
        datatype: 'json',
        data: {
            _token: _token,
            notvalids: JSON.stringify(daftar_validasi_invalid_arr())
        }
    })
    // daftar_validasi_raw_data = JSON.parse(result)
    $('.card-body:has(#daftar-validasi-t) .alert').html('true')
    render('table-daftar-validasi')
    let newToast = new Toast("data validasi terbaru telah tersimpan");
    // return false;
}

async function handlerGetEntrySystem(_token, rab_terpilih_id) {
    const result = await $.ajax({
        url: _url['frontend.proses.syscode.show'](rab_terpilih_id),
        type: "GET",
        datatype: 'json',
        data: {
            _token: _token,
        }
    })
    daftar_kode_sistem_raw_data = JSON.parse(result)
    // $('.card-body:has(#daftar-validasi-t) .alert').html('true')
    render('table-daftar-kode-sistem')
    // return false;
}

async function commitEntrySystemChanges(_token, rab_terpilih_id) {
    const result = await $.ajax({
        url: _url['frontend.proses.syscode.update'](rab_terpilih_id),
        type: "POST",
        datatype: 'json',
        data: {
            _token: _token,
            syscodes: JSON.stringify(
                daftar_kode_sistem_raw_data.map((el) => {return{'id': el.id, 'kode_sistem': el.kode_sistem}})
            )
        }
    })
    // daftar_validasi_raw_data = JSON.parse(result)
    $('.card-body:has(#daftar-kode-sistem-t) .alert').html('true')
    render('table-daftar-kode-sistem')
    let newToast = new Toast("data entri sistem terbaru telah tersimpan");

    // return false;
}

async function handlerGetBOQ(_token, rab_terpilih_id) {
    const result = await $.ajax({
        url: _url['frontend.proses.boq.show'](rab_terpilih_id),
        type: "GET",
        datatype: 'json',
        data: {
            _token: _token,
        }
    })
    boq_raw_data = JSON.parse(result)
    // render('table-daftar-kode-sistem')
}

function getWorkbook() {
    return new Promise(function (resolve, reject) {
        var req = new XMLHttpRequest();
        var url = excel_template_url;
        req.open("GET", url, true);
        req.responseType = "arraybuffer";
        req.onreadystatechange = function () {
            if (req.readyState === 4){
                if (req.status === 200) {
                    resolve(XlsxPopulate.fromDataAsync(req.response));
                } else {
                    reject("Received a " + req.status + " HTTP code.");
                }
            }
        };
        req.send();
    });
}

// _url['frontend.entry.valids.show'](id)

function convertToRoman(num) {
    var roman = {
        M: 1000,
        CM: 900,
        D: 500,
        CD: 400,
        C: 100,
        XC: 90,
        L: 50,
        XL: 40,
        X: 10,
        IX: 9,
        V: 5,
        IV: 4,
        I: 1
    };
    var str = '';

    for (var i of Object.keys(roman)) {
        var q = Math.floor(num / roman[i]);
        num -= q * roman[i];
        str += i.repeat(q);
    }

    return str;
}

</script>
@endpush